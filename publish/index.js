// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const fs = require('fs')
const path = require('path')
const express = require('express')
const handlebars = require('express-handlebars').create({
  defaultLayout: 'main',
  extname: 'html',
  helpers: require('./server/view-helpers')
})
const log = require('bunyan').createLogger({ name: 'server' })
const compression = require('compression')
const hash = require('./server/hash')
const routes = require('./server/routes')
const { WEEK } = require('./server/constants')

const APPLE_57_PATH = path.join(__dirname, './public/apple-touch-icon-57x57.png')
const APPLE_60_PATH = path.join(__dirname, './public/apple-touch-icon-60x60.png')
const APPLE_72_PATH = path.join(__dirname, './public/apple-touch-icon-72x72.png')
const APPLE_76_PATH = path.join(__dirname, './public/apple-touch-icon-76x76.png')
const APPLE_114_PATH = path.join(__dirname, './public/apple-touch-icon-114x114.png')
const APPLE_120_PATH = path.join(__dirname, './public/apple-touch-icon-120x120.png')
const APPLE_144_PATH = path.join(__dirname, './public/apple-touch-icon-144x144.png')
const APPLE_152_PATH = path.join(__dirname, './public/apple-touch-icon-152x152.png')
const FAVICON_ICO_PATH = path.join(__dirname, './public/favicon.ico')
const FAVICON_16_PATH = path.join(__dirname, './public/favicon-16x16.png')
const FAVICON_32_PATH = path.join(__dirname, './public/favicon-32x32.png')
const FAVICON_96_PATH = path.join(__dirname, './public/favicon-96x96.png')
const FAVICON_128_PATH = path.join(__dirname, './public/favicon-128x128.png')
const FAVICON_196_PATH = path.join(__dirname, './public/favicon-196x196.png')
const MSTILE_70_PATH = path.join(__dirname, './public/mstile-70x70.png')
const MSTILE_144_PATH = path.join(__dirname, './public/mstile-144x144.png')
const MSTILE_150_PATH = path.join(__dirname, './public/mstile-150x150.png')
const MSTILE_310_150_PATH = path.join(__dirname, './public/mstile-310x150.png')
const MSTILE_310_PATH = path.join(__dirname, './public/mstile-310x310.png')
const ROBOTS_PATH = path.join(__dirname, './public/robots.txt')
const PATUA_TTF_PATH = path.join(__dirname, './public/fonts/patua-one-v6-latin-regular.ttf')
const PATUA_WOFF_PATH = path.join(__dirname, './public/fonts/patua-one-v6-latin-regular.woff')
const PATUA_WOFF2_PATH = path.join(__dirname, './public/fonts/patua-one-v6-latin-regular.woff2')
const ROBOTO_REGULAR_TTF_PATH = path.join(__dirname, './public/fonts/roboto-v16-latin-regular.ttf')
const ROBOTO_REGULAR_WOFF_PATH = path.join(__dirname, './public/fonts/roboto-v16-latin-regular.woff')
const ROBOTO_REGULAR_WOFF2_PATH = path.join(__dirname, './public/fonts/roboto-v16-latin-regular.woff2')
const ROBOTO_HEAVY_TTF_PATH = path.join(__dirname, './public/fonts/roboto-v16-latin-700.ttf')
const ROBOTO_HEAVY_WOFF_PATH = path.join(__dirname, './public/fonts/roboto-v16-latin-700.woff')
const ROBOTO_HEAVY_WOFF2_PATH = path.join(__dirname, './public/fonts/roboto-v16-latin-700.woff2')
const STYLE_PATH = path.join(__dirname, './public/style/main.css')

const viewData = {
  title: 'Cricket Forecast',
  tagline: 'Predicting cricket results so you don\'t have to',
  hashes: {
    robots: hash(fs.readFileSync(ROBOTS_PATH)),
    apple: {
      png57: hash(fs.readFileSync(APPLE_57_PATH)),
      png60: hash(fs.readFileSync(APPLE_60_PATH)),
      png72: hash(fs.readFileSync(APPLE_72_PATH)),
      png76: hash(fs.readFileSync(APPLE_76_PATH)),
      png114: hash(fs.readFileSync(APPLE_114_PATH)),
      png120: hash(fs.readFileSync(APPLE_120_PATH)),
      png144: hash(fs.readFileSync(APPLE_144_PATH)),
      png152: hash(fs.readFileSync(APPLE_152_PATH))
    },
    favicon: {
      ico: hash(fs.readFileSync(FAVICON_ICO_PATH)),
      png16: hash(fs.readFileSync(FAVICON_16_PATH)),
      png32: hash(fs.readFileSync(FAVICON_32_PATH)),
      png96: hash(fs.readFileSync(FAVICON_96_PATH)),
      png128: hash(fs.readFileSync(FAVICON_128_PATH)),
      png196: hash(fs.readFileSync(FAVICON_196_PATH))
    },
    fonts: {
      patua: {
        woff2: hash(fs.readFileSync(PATUA_WOFF2_PATH)),
        woff: hash(fs.readFileSync(PATUA_WOFF_PATH)),
        ttf: hash(fs.readFileSync(PATUA_TTF_PATH))
      },
      robotoRegular: {
        woff2: hash(fs.readFileSync(ROBOTO_REGULAR_WOFF2_PATH)),
        woff: hash(fs.readFileSync(ROBOTO_REGULAR_WOFF_PATH)),
        ttf: hash(fs.readFileSync(ROBOTO_REGULAR_TTF_PATH))
      },
      robotoHeavy: {
        woff2: hash(fs.readFileSync(ROBOTO_HEAVY_WOFF2_PATH)),
        woff: hash(fs.readFileSync(ROBOTO_HEAVY_WOFF_PATH)),
        ttf: hash(fs.readFileSync(ROBOTO_HEAVY_TTF_PATH))
      }
    },
    mstile: {
      png70: hash(fs.readFileSync(MSTILE_70_PATH)),
      png144: hash(fs.readFileSync(MSTILE_144_PATH)),
      png150: hash(fs.readFileSync(MSTILE_150_PATH)),
      png310150: hash(fs.readFileSync(MSTILE_310_150_PATH)),
      png310: hash(fs.readFileSync(MSTILE_310_PATH))
    },
    style: {
      main: hash(fs.readFileSync(STYLE_PATH))
    }
  },
  copyright: {
    holder: 'Cricket Forecast'
  }
}
const middleware = require('./server/middleware')({
  ...viewData,
  routes: routes.map(route => ({
    path: route.path,
    isLink: true,
    navTitle: route.navTitle
  }))
})

const app = express()

app.engine('html', handlebars.engine)
app.set('view engine', 'html')
app.set('view cache', true)
app.set('trust proxy', true)
app.set('x-powered-by', false)
app.set('etag', false)

app.use(middleware.redirectToCanonicalUrl)
app.use(middleware.logRequest)
app.use(compression())
app.use(middleware.rewriteCacheableUrl)
app.use(middleware.handlePath)
app.use(express.static('public', { maxAge: WEEK, etag: false, lastModified: false }))

routes.forEach(route => {
  const routeViewData = {
    ...viewData,
    routes: routes.map(viewDataRoute => ({
      path: viewDataRoute.path,
      isLink: viewDataRoute.path !== route.path,
      navTitle: viewDataRoute.navTitle
    }))
  }
  app[route.method || 'get'](route.path, (request, response, next) => {
    route.handler(routeViewData, request, response, next)
  })
})

app.use(middleware.handleDynamicRequest)

const port = process.env.PORT || 8080
app.listen(port, () =>
  log.info({ port }, 'listening')
)

