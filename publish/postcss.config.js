// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

module.exports = {
  plugins: [
    require('postcss-import')({
      plugins: [
        require('stylelint')({
          config: {
            extends: 'stylelint-config-standard',
            rules: {
              'custom-property-empty-line-before': null,
              'declaration-colon-newline-after': null,
              'selector-list-comma-newline-after': null,
              'value-list-comma-newline-after': null
            }
          }
        })
      ]
    }),
    require('postcss-cssnext')({
      browsers: '>= 0.2%, last 3 versions, Firefox ESR'
    }),
    require('cssnano')({
      preset: 'advanced'
    })
  ]
}
