// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const check = require('check-types')

module.exports = {
  formatInteger,

  formatFloat (number) {
    if (
      check.not.number(number) ||
      number % 1 === 0 ||
      number.toString() === number.toFixed(1)
    ) {
      return formatInteger(number)
    }

    return formatInteger(Number(number.toFixed(2)))
  },

  add (lhs, rhs) {
    return lhs + rhs
  },

  minus (lhs, rhs) {
    return lhs - rhs
  },

  halve (number) {
    return number / 2
  },

  capitalise (string) {
    return `${string.substr(0, 1).toUpperCase()}${string.substr(1)}`
  },

  plural (number, unit) {
    if (number === 1) {
      return `1 ${unit}`
    }

    return `${formatInteger(number)} ${unit}s`
  },

  debug (...args) {
    console.log('#####   D E B U G   #####')
    console.log('template context:')
    console.log(this)
    if (args.length === 2) {
      console.log('template value:')
      console.log(args[0])
    }
  }
}

function formatInteger (number) {
  if (! number) {
    return number
  }

  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}
