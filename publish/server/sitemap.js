// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const constants = require('./constants')
const data = require('./data')
const hash = require('./hash')
const log = require('bunyan').createLogger({ name: 'server/sitemap' })

const REFRESH_INTERVAL = constants.HOUR

let sitemap, sitemapHash

module.exports = (request, response) => {
  try {
    if (sitemapHash) {
      if (request.headers['if-none-match'] === `W/${sitemapHash}`) {
        response.sendStatus(304)
      } else {
        response.set('ETag', `W/"${sitemapHash}`)
        response.type('text/xml')
        response.status(200)
        response.send(sitemap)
      }
    }
  } catch (error) {
    log.error(error)
    response.sendStatus(404)
  }
}

refreshSitemap().then(() => setInterval(refreshSitemap, REFRESH_INTERVAL))

async function refreshSitemap () {
  try {
    log.info('refreshing sitemap')

    const predictions = await data.get()

    sitemap = `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
    <loc>https://www.cricketforecast.com/</loc>
    <changefreq>daily</changefreq>
    <priority>0.6</priority>
  </url>
  <url>
    <loc>https://www.cricketforecast.com/international</loc>
    <changefreq>daily</changefreq>
  </url>
  <url>
    <loc>https://www.cricketforecast.com/domestic</loc>
    <changefreq>daily</changefreq>
  </url>
  <url>
    <loc>https://www.cricketforecast.com/test</loc>
    <changefreq>daily</changefreq>
  </url>
  <url>
    <loc>https://www.cricketforecast.com/one-day</loc>
    <changefreq>daily</changefreq>
  </url>
  <url>
    <loc>https://www.cricketforecast.com/twenty20</loc>
    <changefreq>daily</changefreq>
  </url>${predictions.map(mapPrediction).join('')}
</urlset>`

    sitemapHash = hash(sitemap)

    log.info('refreshed sitemap')
  } catch (error) {
    log.error(error)
  }
}

function mapPrediction (prediction) {
  const { dateAbbr: date, formatAbbr: format } = prediction
  const teams = `${slug(prediction.home)}-vs-${slug(prediction.away)}`

  return `
  <url>
    <loc>https://www.cricketforecast.com/matches/${format}/${date}/${teams}</loc>
    <changefreq>yearly</changefreq>
  </url>`
}

function slug (string) {
  return string.trim()
    .toLowerCase()
    .replace(/\s+/g, '-')
    .replace(/[^a-z0-9-]/g, '')
}
