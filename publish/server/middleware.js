// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const log = require('bunyan').createLogger({ name: 'server/middleware' })
const routes = require('./routes').reduce((set, route) => set.add(route.path), new Set())
const matchRoutes = require('./match-routes')
const sitemap = require('./sitemap')

const YEAR = require('./constants').YEAR / 1000
const CACHE_FOR_A_YEAR = [
  'public',
  'immutable',
  `max-age=${YEAR}`,
  `stale-while-revalidate=${YEAR}`,
  `stale-if-error=${YEAR}`
].join(', ')

const MATCH_PATH = /^\/matches\/(test|odi|t20i|fc|1d|t20)\/(20[1-9][0-9]-[01][0-9]-[0-3][0-9])\/([a-z0-9-]+-vs-[a-z0-9-]+)\/?$/i

module.exports = viewData => {
  return {
    redirectToCanonicalUrl,
    logRequest,
    rewriteCacheableUrl,
    handlePath,
    handleDynamicRequest
  }

  function handlePath (request, response, next) {
    switch (request.path) {
      case '/apple-touch-icon-57x57.png':
        response.set('ETag', `"${viewData.hashes.apple.png57}"`)
        return next()
      case '/apple-touch-icon-60x60.png':
        response.set('ETag', `"${viewData.hashes.apple.png60}"`)
        return next()
      case '/apple-touch-icon-72x72.png':
        response.set('ETag', `"${viewData.hashes.apple.png72}"`)
        return next()
      case '/apple-touch-icon-76x76.png':
        response.set('ETag', `"${viewData.hashes.apple.png76}"`)
        return next()
      case '/apple-touch-icon-114x114.png':
        response.set('ETag', `"${viewData.hashes.apple.png114}"`)
        return next()
      case '/apple-touch-icon-120x120.png':
        response.set('ETag', `"${viewData.hashes.apple.png120}"`)
        return next()
      case '/apple-touch-icon-144x144.png':
        response.set('ETag', `"${viewData.hashes.apple.png144}"`)
        return next()
      case '/apple-touch-icon-152x152.png':
        response.set('ETag', `"${viewData.hashes.apple.png152}"`)
        return next()
      case '/favicon.ico':
        response.set('ETag', `"${viewData.hashes.favicon.ico}"`)
        return next()
      case '/favicon-16x16.png':
        response.set('ETag', `"${viewData.hashes.favicon.png16}"`)
        return next()
      case '/favicon-32x32.png':
        response.set('ETag', `"${viewData.hashes.favicon.png32}"`)
        return next()
      case '/favicon-96x96.png':
        response.set('ETag', `"${viewData.hashes.favicon.png96}"`)
        return next()
      case '/favicon-128x128.png':
        response.set('ETag', `"${viewData.hashes.favicon.png128}"`)
        return next()
      case '/favicon-196x196.png':
        response.set('ETag', `"${viewData.hashes.favicon.png196}"`)
        return next()
      case '/google37c78afc60247a94.html':
        return next()
      case '/mstile-70x70.png':
        response.set('ETag', `"${viewData.hashes.mstile.png70}"`)
        return next()
      case '/mstile-144x144.png':
        response.set('ETag', `"${viewData.hashes.mstile.png144}"`)
        return next()
      case '/mstile-150x150.png':
        response.set('ETag', `"${viewData.hashes.mstile.png150}"`)
        return next()
      case '/mstile-310x150.png':
        response.set('ETag', `"${viewData.hashes.mstile.png310150}"`)
        return next()
      case '/mstile-310x310.png':
        response.set('ETag', `"${viewData.hashes.mstile.png310}"`)
        return next()
      case '/robots.txt':
        response.set('ETag', `"${viewData.hashes.robots}"`)
        return next()
      case '/sitemap.xml':
        return sitemap(request, response, next)
      case '/fonts/patua-one-v6-latin-regular.ttf':
        response.set('ETag', `"${viewData.hashes.fonts.patua.ttf}"`)
        return next()
      case '/fonts/patua-one-v6-latin-regular.woff':
        response.set('ETag', `"${viewData.hashes.fonts.patua.woff}"`)
        return next()
      case '/fonts/patua-one-v6-latin-regular.woff2':
        response.set('ETag', `"${viewData.hashes.fonts.patua.woff2}"`)
        return next()
      case '/fonts/roboto-v16-latin-regular.ttf':
        response.set('ETag', `"${viewData.hashes.fonts.robotoRegular.ttf}"`)
        return next()
      case '/fonts/roboto-v16-latin-regular.woff':
        response.set('ETag', `"${viewData.hashes.fonts.robotoRegular.woff}"`)
        return next()
      case '/fonts/roboto-v16-latin-regular.woff2':
        response.set('ETag', `"${viewData.hashes.fonts.robotoRegular.woff2}"`)
        return next()
      case '/fonts/roboto-v16-latin-700.ttf':
        response.set('ETag', `"${viewData.hashes.fonts.robotoHeavy.ttf}"`)
        return next()
      case '/fonts/roboto-v16-latin-700.woff':
        response.set('ETag', `"${viewData.hashes.fonts.robotoHeavy.woff}"`)
        return next()
      case '/fonts/roboto-v16-latin-700.woff2':
        response.set('ETag', `"${viewData.hashes.fonts.robotoHeavy.woff2}"`)
        return next()
      case '/style/main.css':
        response.set('ETag', `"${viewData.hashes.style.main}"`)
        return next()
    }

    if (routes.has(request.path)) {
      return next()
    }

    const parts = MATCH_PATH.exec(request.path)
    if (parts && parts.length === 4) {
      return matchRoutes(parts, viewData, request, response, next)
    }

    response.sendStatus(404)
  }
}

function redirectToCanonicalUrl (request, response, next) {
  const isProduction = process.env.NODE_ENV === 'production'
  const host = request.hostname
  const canonicalHost = `${host.startsWith('www.') ? '' : 'www.'}${host}`
  const protocol = request.headers['x-forwarded-proto']

  // eslint-disable-next-line no-extra-parens
  if (! isProduction || (host.length === canonicalHost.length && protocol === 'https')) {
    return next()
  }

  const canonicalUrl = joinUrl('https', canonicalHost, request.path, request.query)

  log.warn({ canonicalUrl }, '301 redirect')

  response.redirect(301, canonicalUrl)
}

function joinUrl (protocol, host, path, query) {
  let queryString = ''

  if (query) {
    const params = Object.keys(query)

    if (params.length > 0) {
      queryString = `?${params}`
        .map(param => `${param}=${query[param]}`)
        .join('&')
    }
  }

  return `${protocol}://${host}${path}${queryString}`
}

function logRequest (request, response, next) {
  log.info({
    method: request.method,
    path: request.path,
    query: request.query,
    body: request.body,
    protocol: request.protocol,
    host: request.hostname,
    proxyIp: request.ip
  }, 'received')

  next()
}

function rewriteCacheableUrl (request, response, next) {
  const url = request.url.replace(/\/([^/]+)\.[0-9a-f]{40}\.([0-9a-z]+)$/i, '/$1.$2')

  if (url.length !== request.url.length) {
    request.url = url
    response.set('Cache-Control', CACHE_FOR_A_YEAR)
  }

  next()
}

function handleDynamicRequest (request, response) {
  const etag = response.locals.etag
  if (etag) {
    if (request.headers['if-none-match'] === `W/${etag}`) {
      response.sendStatus(304)
    } else {
      response.set('ETag', `W/"${etag}"`)
    }
  }

  response.render(response.locals.viewName)
}

