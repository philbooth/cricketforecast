// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const log = require('bunyan').createLogger({ name: 'server/data' })
const { Pool } = require('pg')
const Promise = require('bluebird')
const schema = require('../../ingest/db/schema')
const slug = require('./slug')

const pool = new Pool()
const { teams, matches, formats, innings, ratings, predictions } = schema

const DB_CONTAINS_SANE_FORMATS_FROM = new Date('2018-10-28T00:00:00Z')

const INTERNATIONAL_FORMATS = new Set([ 'Test', 'ODI', 'T20I' ])
const DOMESTIC_FORMATS = new Set([ 'FC', '1D', 'T20' ])
const TEST_FORMATS = new Set([ 'Test' ])
const LIMITED_OVERS_FORMATS = new Set([ '1D', 'ODI', 'T20', 'T20I' ])
const ONE_DAY_FORMATS = new Set([ 'ODI', '1D' ])
const TWENTY20_FORMATS = new Set([ 'T20I', 'T20' ])

const DISPLAY_FORMATS = new Map([
  [ 'Test', 'Test' ],
  [ 'FC', 'First-class' ],
  [ 'ODI', 'One-day international' ],
  [ '1D', 'One-day' ],
  [ 'T20I', 'Twenty20 international' ],
  [ 'T20', 'Twenty20' ]
])

const FORM_HISTORY_LENGTH = 6
const CHART_WIDTH = 402
const CHART_HEIGHT = 68
const CHART_PADDING = 2
const CHART_MAX_BAR_LENGTH = CHART_WIDTH - CHART_PADDING
const CHART_MED_BAR_LENGTH = CHART_MAX_BAR_LENGTH / 2
const CHART_BAR_WIDTH = 40
const CHART_AXIS_WIDTH = 2
const CHART_LABEL_OFFSET = 20
const CHART_BAR_LABEL_OFFSET = 26

module.exports = { get }

async function get () {
  try {
    log.trace('get')

    const [ m, p ] = await Promise.all([ getMatches(), getPredictions() ])

    return p.map(prediction => mapPrediction(prediction, m))
      .reverse()
      .concat(m.map(mapMatch))
  } catch (error) {
    log.error(error)
    return []
  }
}

async function getMatches () {
  log.trace('getMatches')

  const home = teams.as('h')
  const away = teams.as('a')
  const firstInnings = innings.as('i1')
  const secondInnings = innings.as('i2')
  const thirdInnings = innings.as('i3')
  const fourthInnings = innings.as('i4')
  const homeRating = ratings.as('hr')
  const awayRating = ratings.as('ar')
  const tables = matches
    .join(home)
    .on(matches.home.equals(home.id))
    .join(away)
    .on(matches.away.equals(away.id))
    .join(formats)
    .on(matches.format.equals(formats.id))
    .join(firstInnings)
    .on(matches.firstInnings.equals(firstInnings.id))
    .join(secondInnings)
    .on(matches.secondInnings.equals(secondInnings.id))
    .leftJoin(thirdInnings)
    .on(matches.thirdInnings.equals(thirdInnings.id))
    .leftJoin(fourthInnings)
    .on(matches.fourthInnings.equals(fourthInnings.id))
    .leftJoin(homeRating)
    .on(
      home.id.equals(homeRating.team).and(
        matches.id.equals(homeRating.match)
      )
    )
    .leftJoin(awayRating)
    .on(
      away.id.equals(awayRating.team).and(
        matches.id.equals(awayRating.match)
      )
    )
    .join(predictions)
    .on(
      matches.startTime.equals(predictions.startTime)
        .and(matches.format.equals(predictions.format))
        .and(matches.home.equals(predictions.home))
        .and(matches.away.equals(predictions.away))
    )

  const result = await pool.query(
    matches.select(
      matches.id,
      home.name.as('home'),
      away.name.as('away'),
      formats.name.as('format'),
      matches.startTime,
      matches.winner,
      matches.toss,
      matches.batFirst,
      firstInnings.runs.as('i1Runs'),
      firstInnings.wickets.as('i1Wickets'),
      firstInnings.overs.as('i1Overs'),
      secondInnings.runs.as('i2Runs'),
      secondInnings.wickets.as('i2Wickets'),
      secondInnings.overs.as('i2Overs'),
      thirdInnings.runs.as('i3Runs'),
      thirdInnings.wickets.as('i3Wickets'),
      thirdInnings.overs.as('i3Overs'),
      thirdInnings.isFollowingOn.as('i3FollowOn'),
      fourthInnings.runs.as('i4Runs'),
      fourthInnings.wickets.as('i4Wickets'),
      fourthInnings.overs.as('i4Overs'),
      homeRating.rating.as('homeRating'),
      awayRating.rating.as('awayRating'),
      predictions.winner.as('prediction'),
      predictions.confidence
    )
      .from(tables)
      .where(
        matches.isComplete.equals(true)
          .and(matches.startTime.gt(DB_CONTAINS_SANE_FORMATS_FROM.toISOString()))
      )
      .order(matches.startTime.descending)
      .toQuery()
  )

  log.info(`getMatches returning ${result.rows.length} rows`)
  log.debug('getMatches result', result.rows)

  return result.rows
}

async function getPredictions () {
  log.trace('getPredictions')

  const home = teams.as('h')
  const away = teams.as('a')
  const tables = predictions
    .join(home)
    .on(predictions.home.equals(home.id))
    .join(away)
    .on(predictions.away.equals(away.id))
    .join(formats)
    .on(predictions.format.equals(formats.id))
    .leftJoin(matches)
    .on(
      predictions.startTime.equals(matches.startTime)
        .and(predictions.format.equals(matches.format))
        .and(predictions.home.equals(matches.home))
        .and(predictions.away.equals(matches.away))
    )

  const result = await pool.query(
    predictions.select(
      predictions.id,
      home.name.as('home'),
      away.name.as('away'),
      formats.name.as('format'),
      predictions.startTime,
      predictions.winner.as('prediction'),
      predictions.confidence
    )
      .from(tables)
      .where(
        predictions.startTime.gte(now())
          .and(matches.id.isNull())
      )
      .order(predictions.startTime.ascending)
      .toQuery()
  )

  log.info(`getPredictions returning ${result.rows.length} rows`)
  log.debug('getPredictions result', result.rows)

  return result.rows
}

function now () {
  const time = new Date()

  const year = time.getUTCFullYear()
  const month = time.getUTCMonth() + 1
  const day = time.getUTCDate()
  const hour = time.getUTCHours()
  const minute = time.getUTCMinutes()
  const second = time.getUTCSeconds()

  return `${year}-${pad(month)}-${pad(day)} ${pad(hour)}:${pad(minute)}:${pad(second)}`
}

function pad (number) {
  if (number < 10) {
    return `0${number}`
  }

  return number
}

function mapPrediction (prediction, matchArray) {
  const date = new Date(prediction.startTime)
  const dateAbbr = marshallDateAbbr(date)
  const formatAbbr = prediction.format.toLowerCase()

  return {
    ...prediction,
    date: date.toUTCString(),
    dateAbbr,
    format: DISPLAY_FORMATS.get(prediction.format),
    formatAbbr,
    isConfident: isConfidentPrediction(prediction),
    isDomestic: DOMESTIC_FORMATS.has(prediction.format),
    isInternational: INTERNATIONAL_FORMATS.has(prediction.format),
    isLimitedOvers: LIMITED_OVERS_FORMATS.has(prediction.format),
    isOneDay: ONE_DAY_FORMATS.has(prediction.format),
    isResult: false,
    isTest: TEST_FORMATS.has(prediction.format),
    isTwenty20: TWENTY20_FORMATS.has(prediction.format),
    path: marshallPath(formatAbbr, dateAbbr, prediction.home, prediction.away),
    prediction: marshallPrediction(prediction),
    timestamp: date.getTime(),
    form: {
      home: mapForm(prediction.home, prediction.format, date, matchArray),
      away: mapForm(prediction.away, prediction.format, date, matchArray)
    }
  }
}

function marshallDateAbbr (date) {
  return `${date.getUTCFullYear()}-${pad(date.getUTCMonth() + 1)}-${pad(date.getUTCDate())}`
}

function isConfidentPrediction (match) {
  return match.confidence > 50
}

function marshallPath (formatAbbr, dateAbbr, home, away) {
  return `/matches/${formatAbbr}/${dateAbbr}/${slug(home)}-vs-${slug(away)}`
}

function marshallPrediction (match) {
  const { prediction, home, away } = match

  if (isConfidentPrediction(match)) {
    if (prediction === 'home') {
      return `${home} win`
    }

    return `${away} win`
  }

  return 'uncertain'
}

function mapForm (team, format, date, matchArray) {
  const charts = []

  for (const match of matchArray) {
    if (charts.length === FORM_HISTORY_LENGTH) {
      break
    }

    if (match.format !== format) {
      continue
    }

    if (new Date(match.startTime) >= date) {
      continue
    }

    const isHome = match.home === team
    const isAway = match.away === team

    if (isHome || isAway) {
      const { score: homeScore, aggregate: homeAggregate } = marshallScore(match, 'home')
      const { score: awayScore, aggregate: awayAggregate } = marshallScore(match, 'away')
      const homeWeighting = weightHomePerformance(homeAggregate, awayAggregate, match.winner)
      const homeLength = CHART_MAX_BAR_LENGTH * homeWeighting
      const awayLength = CHART_MAX_BAR_LENGTH - homeLength

      let homeOrAway, otherTeam, homeClass, awayClass

      if (isHome) {
        homeOrAway = 'Home'
        otherTeam = match.away
        homeClass = 'this-team'
        awayClass = 'other-team'
      } else {
        homeOrAway = 'Away'
        otherTeam = match.home
        homeClass = 'other-team'
        awayClass = 'this-team'
      }

      charts.push({
        width: CHART_WIDTH,
        height: CHART_HEIGHT,
        barWidth: CHART_BAR_WIDTH,
        axisWidth: CHART_AXIS_WIDTH,
        padding: CHART_PADDING,
        labelOffset: CHART_LABEL_OFFSET,
        barLabelOffset: CHART_BAR_LABEL_OFFSET,
        homeOrAway,
        otherTeam,
        homeTeam: match.home,
        homeLength,
        homeClass,
        homeScore,
        awayTeam: match.away,
        awayLength,
        awayClass,
        awayScore,
      })
    }
  }

  return {
    team,
    charts,
    hasCharts: charts.length > 0
  }
}

function marshallScore (match, side) {
  const batFirst = match.batFirst === side

  let score, runs, wickets

  if (batFirst) {
    score = `${match.i1Runs}/${match.i1Wickets}`
    runs = match.i1Runs
    wickets = match.i1Wickets
  } else {
    score = `${match.i2Runs}/${match.i2Wickets}`
    runs = match.i2Runs
    wickets = match.i2Wickets
  }

  if (typeof match.i3Runs === 'number') {
    if (batFirst || match.i3FollowOn) {
      score += ` & ${match.i3Runs}/${match.i3Wickets}`
      runs += match.i3Runs
      wickets += match.i3Wickets
    } else if (typeof match.i4Runs === 'number') {
      score += ` & ${match.i4Runs}/${match.i4Wickets}`
      runs += match.i4Runs
      wickets += match.i4Wickets
    }
  }

  let aggregate = runs - wickets * 5

  if (aggregate <= 0) {
    aggregate = 1
  }

  return { score, aggregate }
}

function weightHomePerformance (homeAggregate, awayAggregate, winner) {
  let homeWeighting = homeAggregate / awayAggregate
  let awayWeighting = awayAggregate / homeAggregate

  if (winner === 'home') {
    if (homeWeighting <= 1) {
      homeWeighting = 1.1
      awayWeighting = 0.9
    }
  } else if (winner === 'away') {
    if (homeWeighting >= 1) {
      homeWeighting = 0.9
    }
  } else {
    homeWeighting = 1
  }

  if (homeWeighting <= 1) {
    return homeWeighting / 2
  }

  return 1 - awayWeighting / 2
}

function mapMatch (match, index, matchArray) {
  const date = new Date(match.startTime)
  const dateAbbr = marshallDateAbbr(date)
  const formatAbbr = match.format.toLowerCase()

  return {
    ...match,
    date: date.toUTCString(),
    dateAbbr,
    format: DISPLAY_FORMATS.get(match.format),
    formatAbbr,
    isConfident: isConfidentPrediction(match),
    isCorrect: match.winner === match.prediction,
    isDomestic: DOMESTIC_FORMATS.has(match.format),
    isInternational: INTERNATIONAL_FORMATS.has(match.format),
    isLimitedOvers: LIMITED_OVERS_FORMATS.has(match.format),
    isOneDay: ONE_DAY_FORMATS.has(match.format),
    isResult: true,
    isTest: TEST_FORMATS.has(match.format),
    isTwenty20: TWENTY20_FORMATS.has(match.format),
    path: marshallPath(formatAbbr, dateAbbr, match.home, match.away),
    prediction: marshallPrediction(match),
    result: marshallResult(match),
    timestamp: date.getTime(),
    form: {
      home: mapForm(match.home, match.format, date, matchArray),
      away: mapForm(match.away, match.format, date, matchArray)
    }
  }
}

function marshallResult (match) {
  const { winner, home, away, format, i3Runs, i3FollowOn, i4Runs, i4Wickets } = match
  let { i1Runs, i2Runs } = match

  if (winner === 'home') {
    return `${home} won`
  }

  if (winner === 'away') {
    return `${away} won`
  }

  if (LIMITED_OVERS_FORMATS.has(format)) {
    if (i1Runs === i2Runs) {
      return 'match tied'
    }

    return 'no result'
  }

  if (i3FollowOn) {
    i2Runs += i3Runs
    i1Runs += i4Runs
  } else {
    i1Runs += i3Runs
    i2Runs += i4Runs
  }

  if (i1Runs === i2Runs && i4Wickets === 10) {
    return 'match tied'
  }

  return 'match drawn'
}
