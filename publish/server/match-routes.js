// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const constants = require('./constants')
const data = require('./data')
const hash = require('./hash')
const log = require('bunyan').createLogger({ name: 'server/match-routes' })
const slug = require('./slug')

const REFRESH_INTERVAL = constants.HOUR

let predictions

module.exports = ([ , format, date, teams ], viewData, request, response, next) => {
  try {
    const { prediction, hash: etag } = predictions[format.toLowerCase()][date][teams.toLowerCase()]
    console.log(prediction)

    response.locals = {
      ...viewData,
      ...prediction,
      viewName: 'match',
      viewTitle: `${prediction.home} vs ${prediction.away}`,
      appendSiteTitle: true,
      description: `Analysis and prediction for the ${prediction.home} vs ${prediction.away} ${prediction.format} cricket match`,
      etag
    }

    return next()
  } catch (error) {
    log.error(error)
    response.sendStatus(404)
  }
}

refreshPredictions().then(() => setInterval(refreshPredictions, REFRESH_INTERVAL))

async function refreshPredictions () {
  try {
    log.info('refreshing predictions')

    const rawPredictions = await data.get()

    predictions = rawPredictions.reduce((indexed, prediction) => {
      const { dateAbbr: date, formatAbbr: format } = prediction

      if (! indexed[format]) {
        indexed[format] = {}
      }

      if (! indexed[format][date]) {
        indexed[format][date] = {}
      }

      const teams = `${slug(prediction.home)}-vs-${slug(prediction.away)}`
      const datum = {
        prediction,
        hash: hash(JSON.stringify(prediction))
      }

      indexed[format][date][teams] = datum

      return indexed
    }, {})

    log.info('refreshed predictions')
  } catch (error) {
    log.error(error)
  }
}
