// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const constants = require('./constants')
const data = require('./data')
const hash = require('./hash')
const log = require('bunyan').createLogger({ name: 'server/routes' })

const REFRESH_INTERVAL = constants.HOUR
const ROUTES = [
  { path: '/', navTitle: 'All', description: 'Predicting cricket results so you don\'t have to' },
  { path: '/international', navTitle: 'International', filter: prediction => prediction.isInternational },
  { path: '/domestic', navTitle: 'Domestic', filter: prediction => prediction.isDomestic },
  { path: '/test', navTitle: 'Test', filter: prediction => prediction.isTest },
  { path: '/one-day', navTitle: 'One-day', filter: prediction => prediction.isOneDay },
  { path: '/twenty20', navTitle: 'Twenty20', filter: prediction => prediction.isTwenty20 }
]
const MAX_ITEMS = 100

let predictions, hashes

module.exports = ROUTES.map(route => ({
  ...route,
  handler: (viewData, request, response, next) => {
    response.locals = {
      ...viewData,
      predictions: predictions[route.path],
      viewName: 'matches',
      viewTitle: `${route.navTitle} predictions`,
      appendSiteTitle: true,
      description: route.description || `${route.navTitle} cricket match result predictions`,
      showTweets: true,
      etag: hashes[route.path]
    }
    next()
  }
}))

refreshPredictions().then(() => setInterval(refreshPredictions, REFRESH_INTERVAL))

async function refreshPredictions () {
  try {
    log.info('refreshing predictions')

    const allPredictions = await data.get()
    predictions = {}
    hashes = {}

    ROUTES.forEach(route => {
      const { filter, path } = route

      let routePredictions
      if (filter) {
        routePredictions = allPredictions.filter(filter)
      } else {
        routePredictions = allPredictions
      }

      predictions[path] = routePredictions.slice(0, MAX_ITEMS)
      hashes[path] = hash(JSON.stringify(routePredictions))
    })

    log.info('refreshed predictions')
  } catch (error) {
    log.error(error)
  }
}
