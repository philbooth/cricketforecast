CREATE OR REPLACE FUNCTION teams ()
RETURNS TABLE (name VARCHAR(128), abbr VARCHAR(8), id UUID)
AS $$
  SELECT name, abbr, id
  FROM teams
  ORDER BY name;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION team (UUID)
RETURNS TABLE (
  name VARCHAR(128),
  abbr VARCHAR(8),
  cricapi INT,
  cricbuzz INT,
  form VARCHAR(34),
  ratings VARCHAR(34)
)
AS $$
  SELECT
    t.name,
    t.abbr,
    t."cricapiId",
    t."cricbuzzId",
    (
      SELECT STRING_AGG(result, ', ')
      FROM (
        SELECT
          CASE
            WHEN winner IS NULL THEN 'tied'
            WHEN home = t.id AND winner = 'home' THEN 'won'
            WHEN home = t.id AND winner = 'away' THEN 'lost'
            WHEN away = t.id AND winner = 'away' THEN 'won'
            WHEN away = t.id AND winner = 'home' THEN 'lost'
          END AS result
        FROM matches
        WHERE (home = t.id OR away = t.id)
          AND "isComplete" = TRUE
        ORDER BY "startTime" DESC
        LIMIT 6
      ) AS results
    ),
    (
      SELECT STRING_AGG(CAST(rating AS TEXT), ', ')
      FROM (
        SELECT rating
        FROM ratings AS r
        INNER JOIN matches AS m ON r.match = m.id
        WHERE r.team = t.id
        ORDER BY m."startTime" DESC
        LIMIT 6
      ) AS ratings
    )
  FROM teams AS t
  WHERE id = $1
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION matches ()
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  result BOOLEAN,
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    m."startTime",
    m."isComplete",
    hr.rating,
    ar.rating,
    m.id
  FROM matches AS m
  INNER JOIN teams AS h ON m.home = h.id
  INNER JOIN teams AS a ON m.away = a.id
  INNER JOIN formats AS f ON m.format = f.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  ORDER BY m."startTime";
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION latest_matches (INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  result BOOLEAN,
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    m."startTime",
    m."isComplete",
    hr.rating,
    ar.rating,
    m.id
  FROM matches AS m
  INNER JOIN teams AS h ON m.home = h.id
  INNER JOIN teams AS a ON m.away = a.id
  INNER JOIN formats AS f ON m.format = f.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  ORDER BY m."startTime" DESC
  LIMIT $1;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION latest_matches (UUID, INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  result BOOLEAN,
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    m."startTime",
    m."isComplete",
    hr.rating,
    ar.rating,
    m.id
  FROM matches AS m
  INNER JOIN teams AS h ON m.home = h.id
  INNER JOIN teams AS a ON m.away = a.id
  INNER JOIN formats AS f ON m.format = f.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  WHERE h.id = $1 OR a.id = $1
  ORDER BY m."startTime" DESC
  LIMIT $2;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION results ()
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  winner CHAR(4),
  toss CHAR(4),
  bat_1 CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    m."startTime",
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating,
    m.id
  FROM matches AS m
  INNER JOIN teams AS h ON m.home = h.id
  INNER JOIN teams AS a ON m.away = a.id
  INNER JOIN formats AS f ON m.format = f.id
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  ORDER BY m."startTime";
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION latest_results (INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  winner CHAR(4),
  toss CHAR(4),
  bat_1 CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    m."startTime",
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating,
    m.id
  FROM matches AS m
  INNER JOIN teams AS h ON m.home = h.id
  INNER JOIN teams AS a ON m.away = a.id
  INNER JOIN formats AS f ON m.format = f.id
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  ORDER BY m."startTime" DESC
  LIMIT $1;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION latest_results (UUID, INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  winner CHAR(4),
  toss CHAR(4),
  bat_1 CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    m."startTime",
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating,
    m.id
  FROM matches AS m
  INNER JOIN teams AS h ON m.home = h.id
  INNER JOIN teams AS a ON m.away = a.id
  INNER JOIN formats AS f ON m.format = f.id
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  WHERE h.id = $1 OR a.id = $1
  ORDER BY m."startTime" DESC
  LIMIT $2;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION tied_results ()
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  winner CHAR(4),
  bat_1 CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    m."startTime",
    m.winner,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating,
    m.id
  FROM matches AS m
  INNER JOIN teams AS h ON m.home = h.id
  INNER JOIN teams AS a ON m.away = a.id
  INNER JOIN formats AS f ON m.format = f.id
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  WHERE m.winner IS NULL
  ORDER BY m."startTime";
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION result (UUID)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  winner CHAR(4),
  toss_winner CHAR(4),
  bat_first CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    m."startTime",
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating
  FROM matches AS m
  INNER JOIN teams AS h ON m.home = h.id
  INNER JOIN teams AS a ON m.away = a.id
  INNER JOIN formats AS f ON m.format = f.id
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  WHERE m.id = $1;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION test_result (UUID)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  winner CHAR(4),
  toss_winner CHAR(4),
  bat_first CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  inns_3 VARCHAR(16),
  inns_4 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    m."startTime",
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    FORMAT('%s/%s (%s)', ti.runs, ti.wickets, ti.overs),
    FORMAT('%s/%s (%s)', li.runs, li.wickets, li.overs),
    hr.rating,
    ar.rating
  FROM matches AS m
  INNER JOIN teams AS h ON m.home = h.id
  INNER JOIN teams AS a ON m.away = a.id
  INNER JOIN formats AS f ON m.format = f.id
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN innings AS ti ON m."thirdInnings" = ti.id
  LEFT JOIN innings AS li ON m."fourthInnings" = li.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  WHERE m.id = $1;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION predictions ()
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  winner CHAR(4),
  confidence SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  ORDER BY p."startTime";
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION latest_predictions (INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  winner CHAR(4),
  confidence SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  ORDER BY p."startTime" DESC
  LIMIT $1;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION latest_predictions (UUID, INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  winner CHAR(4),
  confidence SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  WHERE h.id = $1 OR a.id = $1
  ORDER BY p."startTime" DESC
  LIMIT $2;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION next_predictions (INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  winner CHAR(4),
  confidence SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  WHERE p."startTime"::DATE >= CURRENT_DATE
  AND confidence > 0
  ORDER BY p."startTime"
  LIMIT $1;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION next_predictions (UUID, INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  winner CHAR(4),
  confidence SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  WHERE (h.id = $1 OR a.id = $1)
  AND p."startTime"::DATE >= CURRENT_DATE
  ORDER BY p."startTime"
  LIMIT $2;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION tied_predictions ()
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  WHERE p.confidence = 0
  ORDER BY p."startTime";
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION historical_predictions ()
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  predicted_winner CHAR(4),
  confidence SMALLINT,
  actual_winner CHAR(4),
  toss CHAR(4),
  bat_1 CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  INNER JOIN matches AS m ON p.home = m.home
    AND p.away = m.away AND p."startTime" = m."startTime" AND p.format = m.format
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  ORDER BY p."startTime";
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION latest_historical_predictions (INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  predicted_winner CHAR(4),
  confidence SMALLINT,
  actual_winner CHAR(4),
  toss CHAR(4),
  bat_1 CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  INNER JOIN matches AS m ON p.home = m.home
    AND p.away = m.away AND p."startTime" = m."startTime" AND p.format = m.format
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  ORDER BY p."startTime" DESC
  LIMIT $1;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION latest_historical_predictions (UUID, INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  predicted_winner CHAR(4),
  confidence SMALLINT,
  actual_winner CHAR(4),
  toss CHAR(4),
  bat_1 CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  INNER JOIN matches AS m ON p.home = m.home
    AND p.away = m.away AND p."startTime" = m."startTime" AND p.format = m.format
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  WHERE h.id = $1 OR a.id = $1
  ORDER BY p."startTime" DESC
  LIMIT $2;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION wrong_predictions ()
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  predicted_winner CHAR(4),
  confidence SMALLINT,
  actual_winner CHAR(4),
  toss CHAR(4),
  bat_1 CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  INNER JOIN matches AS m ON p.home = m.home
    AND p.away = m.away AND p."startTime" = m."startTime" AND p.format = m.format
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  WHERE p.winner != m.winner
  ORDER BY p."startTime";
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION latest_wrong_predictions (INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  predicted_winner CHAR(4),
  confidence SMALLINT,
  actual_winner CHAR(4),
  toss CHAR(4),
  bat_1 CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  INNER JOIN matches AS m ON p.home = m.home
    AND p.away = m.away AND p."startTime" = m."startTime" AND p.format = m.format
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  WHERE p.winner != m.winner
  ORDER BY p."startTime" DESC
  LIMIT $1;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION latest_wrong_predictions (UUID, INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  predicted_winner CHAR(4),
  confidence SMALLINT,
  actual_winner CHAR(4),
  toss CHAR(4),
  bat_1 CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  INNER JOIN matches AS m ON p.home = m.home
    AND p.away = m.away AND p."startTime" = m."startTime" AND p.format = m.format
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  WHERE (h.id = $1 OR a.id = $1)
    AND p.winner != m.winner
  ORDER BY p."startTime" DESC
  LIMIT $2;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION correct_predictions ()
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  predicted_winner CHAR(4),
  confidence SMALLINT,
  actual_winner CHAR(4),
  toss CHAR(4),
  bat_1 CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  INNER JOIN matches AS m ON p.home = m.home
    AND p.away = m.away AND p."startTime" = m."startTime" AND p.format = m.format
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  WHERE p.winner = m.winner
  ORDER BY p."startTime";
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION latest_correct_predictions (INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  predicted_winner CHAR(4),
  confidence SMALLINT,
  actual_winner CHAR(4),
  toss CHAR(4),
  bat_1 CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  INNER JOIN matches AS m ON p.home = m.home
    AND p.away = m.away AND p."startTime" = m."startTime" AND p.format = m.format
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  WHERE p.winner = m.winner
  ORDER BY p."startTime" DESC
  LIMIT $1;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION latest_correct_predictions (UUID, INT)
RETURNS TABLE (
  home VARCHAR(128),
  away VARCHAR(128),
  format VARCHAR(32),
  start_time TIMESTAMP,
  predicted_winner CHAR(4),
  confidence SMALLINT,
  actual_winner CHAR(4),
  toss CHAR(4),
  bat_1 CHAR(4),
  inns_1 VARCHAR(16),
  inns_2 VARCHAR(16),
  home_rating SMALLINT,
  away_rating SMALLINT,
  id UUID
)
AS $$
  SELECT
    h.name,
    a.name,
    f.name,
    p."startTime",
    p.winner,
    p.confidence,
    m.winner,
    m.toss,
    m."batFirst",
    FORMAT('%s/%s (%s)', fi.runs, fi.wickets, fi.overs),
    FORMAT('%s/%s (%s)', si.runs, si.wickets, si.overs),
    hr.rating,
    ar.rating,
    p.id
  FROM predictions AS p
  INNER JOIN teams AS h ON p.home = h.id
  INNER JOIN teams AS a ON p.away = a.id
  INNER JOIN formats AS f ON p.format = f.id
  INNER JOIN matches AS m ON p.home = m.home
    AND p.away = m.away AND p."startTime" = m."startTime" AND p.format = m.format
  INNER JOIN innings AS fi ON m."firstInnings" = fi.id
  INNER JOIN innings AS si ON m."secondInnings" = si.id
  LEFT JOIN ratings AS hr ON m.id = hr.match AND h.id = hr.team
  LEFT JOIN ratings AS ar ON m.id = ar.match AND a.id = ar.team
  WHERE (h.id = $1 OR a.id = $1)
    AND p.winner = m.winner
  ORDER BY p."startTime" DESC
  LIMIT $2;
$$
LANGUAGE SQL;
