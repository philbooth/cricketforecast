// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const fs = require('fs')
const path = require('path')
const Promise = require('bluebird')
const request = require('request-promise')
const xml2js = require('xml2js')

fs.readFileP = Promise.promisify(fs.readFile)
xml2js.parseStringP = Promise.promisify(xml2js.parseString)

const USE_FIXTURE = process.env.NODE_ENV === 'development'
const URL = 'http://synd.cricbuzz.com/j2me/1.0/livematches.xml'
const FORMATS = new Map([
  [ 'TEST', 'Test' ]
])
const LIMITED_OVERS = new Map([
  [ 'T20', 20 ],
  [ '1D', 50 ]
  // TODO: other formats
])

module.exports = {
  fetch () {
    return Promise.resolve()
      .then(() => {
        if (USE_FIXTURE) {
          return fs.readFileP(path.join(__dirname, 'fixtures/matches.xml'))
        }

        return request(URL)
      })
      .then(xml => xml2js.parseStringP(xml))
      .then(data => data.mchdata.match
        .map(match => {
          if (match.mscr) {
            return marshallMatchWithScore(match)
          }

          return marshallMatchWithoutScore(match)
        })
      )
      .catch(() => {
        //console.error('cricbuzz.fetch error:', error.message)
        return []
      })
  }
}

function marshallMatchWithScore (match) {
  const state = match.state[0].$
  const time = match.Tme[0].$
  const home = match.Tm[0].$
  const away = match.Tm[1].$
  const format = FORMATS.get(match.$.type) || match.$.type || null
  const batFirst = calculateBatFirst(state)
  let score
  try {
    score = parseScores(home, away, batFirst, match.mscr[0])
  } catch (error) {
    console.error('cricbuzz score-parsing error:', error.message)
  }
  return {
    time: Date.parse(`${time.Dt} ${time.stTme}Z`),
    home: parseTeam(home),
    away: parseTeam(away),
    format,
    isComplete: state.mchState === 'complete',
    toss: calculateTossWinner(state, home, away),
    batFirst,
    winner: score && calculateWinner(format, score, batFirst),
    score
  }
}

function marshallMatchWithoutScore (match) {
  const time = match.Tme[0].$
  const home = parseTeam(match.Tm[0].$)
  const away = parseTeam(match.Tm[1].$)
  const format = FORMATS.get(match.$.type) || match.$.type || null

  return {
    time: Date.parse(`${time.Dt} ${time.stTme}Z`),
    home,
    away,
    format
  }
}

function calculateBatFirst (state) {
  const { decisn: decision } = state

  if (decision === 'Batting') {
    return 'home'
  }

  if (decision === 'Fielding') {
    return 'away'
  }

  console.error('unhandled cricbuzz toss decision:', decision)

  return null
}

function parseScores (home, away, batFirst, score) {
  const scores = [
    parseScore(score.btTm[0]),
    parseScore(score.blgTm[0])
  ]

  let homeIndex = 0, awayIndex = 1
  if (home.id === scores[1].cricbuzzId) {
    homeIndex = 1
    awayIndex = 0
  }

  switch (batFirst) {
    case 'home':
      scores[homeIndex].batFirst = true
      scores[awayIndex].batFirst = false
      break
    case 'away':
      scores[awayIndex].batFirst = true
      scores[homeIndex].batFirst = false
  }

  return {
    home: scores[homeIndex],
    away: scores[awayIndex]
  }
}

function parseScore (team) {
  if (team && team.Inngs) {
    const totals = {
      runs: 0,
      wickets: 0,
      overs: 0
    }

    return Object.assign({
      cricbuzzId: team.$.id,
      totals,
      innings: team.Inngs.map(innings => {
        const runs = parseInt(innings.$.r)
        const wickets = parseInt(innings.$.wkts)
        const overs = parseFloat(innings.$.ovrs)

        totals.runs += runs
        totals.wickets += wickets
        totals.overs += overs

        return {
          runs,
          overs,
          wickets,
          isDeclared: innings.Decl === '1',
          isFollowingOn: innings.FollowOn === '1'
        }
      })
    })
  }
}

function parseTeam (team) {
  let id = parseInt(team.id)
  if (isNaN(id)) {
    id = null
  }

  return {
    name: team.Name.trim(),
    abbr: team.sName.trim(),
    cricbuzzId: id
  }
}

function calculateTossWinner (state, home, away) {
  if (state.TW === home.Name) {
    return 'home'
  }

  if (state.TW === away.Name) {
    return 'away'
  }

  return null
}

function calculateWinner (format, score, batFirst) {
  const { home, away } = score
  const overs = LIMITED_OVERS.get(format)
  if (overs) {
    home.isComplete = isLimitedOversScoreComplete(home, away, overs, batFirst === 'home')
    away.isComplete = isLimitedOversScoreComplete(away, home, overs, batFirst === 'away')
  } else {
    // HACK: temporary logging to help us understand the possible values here
    console.error('non-limited overs cricbuzz format:', format)
    home.isComplete = isFirstClassScoreComplete(home, away, batFirst === 'home')
    away.isComplete = isFirstClassScoreComplete(away, home, batFirst === 'away')
  }

  if (home.isComplete && away.isComplete) {
    if (home.totals.runs > away.totals.runs) {
      return 'home'
    }

    if (away.totals.runs > home.totals.runs) {
      return 'away'
    }

    return 'tie'
  }

  return null
}

function isLimitedOversScoreComplete (score, otherScore, overs, batFirst) {
  // TODO: Duckworth-Lewis
  return score.totals.wickets === 10 ||
    score.totals.overs === overs ||
    (! batFirst && score.totals.runs > otherScore.totals.runs)
}

function isFirstClassScoreComplete (score, otherScore, batFirst) {
  const { length } = score.innings
  return score.innings.reduce((isComplete, innings, index) => {
    return isComplete && (
      innings.isDeclared ||
      innings.wickets === 10 || (
        ! batFirst &&
        index === length - 1 &&
        otherScore.innings.length === length &&
        score.totals.runs > otherScore.totals.runs
      )
    )
  }, true)
}
