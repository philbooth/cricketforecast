// Copyright © 2018 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

/* eslint-disable camelcase */

'use strict'

const {
  TWITTER_CONSUMER_KEY: consumer_key,
  TWITTER_CONSUMER_SECRET: consumer_secret,
  TWITTER_ACCESS_TOKEN_KEY: access_token_key,
  TWITTER_ACCESS_TOKEN_SECRET: access_token_secret
} = process.env

const Promise = require('bluebird')
const twitter = require('twitter')({
  consumer_key,
  consumer_secret,
  access_token_key,
  access_token_secret
})
const vagueTime = require('vague-time')

const VERSION = /^v([0-9]+)$/

twitter.postAsync = Promise.promisify(twitter.post, { context: twitter })

setImmediate(reportUpdate)

module.exports = {
  predictResult (match, prediction) {
    const home = match.home.name
    const away = match.away.name
    const format = mapFormat(match.format)
    const date = new Date(match.time).toUTCString()

    if (prediction.confidence <= 50) {
      return tweet(`I'm not confident enough to predict a winner of the ${
        format} between ${home} and ${away} starting ${date}. 😕`)
    }

    let winner, loser
    if (prediction.winner === 'home') {
      winner = home
      loser = away
    } else {
      winner = away
      loser = home
    }

    return tweet(`Prediction: ${winner} will beat ${loser} in the ${
      format} 🏏 starting ${date}\nConfidence: ${prediction.confidence}%`)
  },

  reportResult (match, prediction) {
    if (! prediction || prediction.confidence <= 50) {
      return Promise.resolve()
    }

    if (! prediction.tweetId) {
      console.log('missing tweetId for prediction:', prediction)
      return Promise.resolve()
    }

    if (prediction.winner === match.winner) {
      return tweet(`Told you! 🦊 https://twitter.com/CricketForecast/status/${prediction.tweetId}`)
    }

    return tweet(`What a chump! 😞 https://twitter.com/CricketForecast/status/${prediction.tweetId}`)
  }
}

function mapFormat (format) {
  switch (format) {
    case 'T20I':
      return 'T20 international'

    case 'T20':
      return 'T20'

    case 'ODI':
      return 'ODI'

    case 'Test':
      return 'test match'

    case '1D':
      return '1-dayer'

    case 'FC':
      return 'first-class match'

    default:
      return 'match'
  }
}

function tweet (status) {
  return twitter.postAsync('statuses/update', { status })
}

function reportUpdate () {
  const version = VERSION.exec(process.env.HEROKU_RELEASE_VERSION)
  if (! version || ! version[1]) {
    return
  }

  const currentVersion = parseInt(version[1])
  const db = require('../db')

  db.readMetadata('version')
    .then(result => {
      const previousVersion = parseInt(result.value)
      if (previousVersion >= currentVersion) {
        return
      }

      const age = vagueTime.get({ to: Date.UTC(2018, 7, 29) })
      const sentences = [
        'My algorithm just got updated!',
        `It's now in its ${pastTenseNumber(currentVersion)} generation.`,
        `I'm ${age.replace('ago', 'old')}.`,
        '🧠'
      ]

      return tweet(sentences.join(' '))
        .then(() => db.writeMetadata('version', `${currentVersion}`))
    })
    .catch(error => console.error(error.stack))
}

function pastTenseNumber (number) {
  const numericString = `${number}`
  switch (numericString[numericString.length - 1]) {
    case '0':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      return `${numericString}th`

    case '1':
      return `${numericString}st`

    case '2':
      return `${numericString}nd`

    case '3':
      return `${numericString}rd`

    default:
      throw new TypeError(`invalid numeric string "${numericString}"`)
  }
}
