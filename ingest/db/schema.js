// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const sql = require('sql')

sql.setDialect('postgres')

module.exports = {
  teams: sql.define({
    name: 'teams',
    columns: [
      { name: 'id', dataType: 'UUID', primaryKey: true, notNull: true },
      { name: 'name', dataType: 'VARCHAR(128)', unique: true, notNull: true },
      { name: 'abbr', dataType: 'VARCHAR(8)' },
      { name: 'cricapiId', dataType: 'INTEGER', unique: true },
      { name: 'cricbuzzId', dataType: 'INTEGER', unique: true }
    ]
  }),

  formats: sql.define({
    name: 'formats',
    columns: [
      { name: 'id', dataType: 'SERIAL', primaryKey: true, notNull: true },
      { name: 'name', dataType: 'VARCHAR(32)', unique: true, notNull: true },
      { name: 'innings', dataType: 'INTEGER' },
      { name: 'overs', dataType: 'INTEGER' }
    ]
  }),

  innings: sql.define({
    name: 'innings',
    columns: [
      { name: 'id', dataType: 'UUID', primaryKey: true, notNull: true },
      {
        name: 'team',
        dataType: 'UUID',
        notNull: true,
        references: { table: 'teams', column: 'id', onDelete: 'cascade' }
      },
      { name: 'runs', dataType: 'SMALLINT', notNull: true },
      { name: 'wickets', dataType: 'SMALLINT', notNull: true },
      { name: 'overs', dataType: 'NUMERIC(4, 1)' },
      { name: 'isDeclared', dataType: 'BOOLEAN' },
      { name: 'isFollowingOn', dataType: 'BOOLEAN' },
    ]
  }),

  matches: sql.define({
    name: 'matches',
    columns: [
      { name: 'id', dataType: 'UUID', primaryKey: true, notNull: true },
      { name: 'startTime', dataType: 'TIMESTAMP', notNull: true },
      {
        name: 'format',
        dataType: 'SERIAL',
        notNull: true,
        references: { table: 'formats', column: 'id', onDelete: 'restrict' }
      },
      {
        name: 'home',
        dataType: 'UUID',
        notNull: true,
        references: { table: 'teams', column: 'id', onDelete: 'cascade' }
      },
      {
        name: 'away',
        dataType: 'UUID',
        notNull: true,
        references: { table: 'teams', column: 'id', onDelete: 'cascade' }
      },
      { name: 'toss', dataType: 'CHAR(4)' },
      { name: 'batFirst', dataType: 'CHAR(4)' },
      { name: 'winner', dataType: 'CHAR(4)' },
      { name: 'isComplete', dataType: 'BOOLEAN' },
      {
        name: 'firstInnings',
        dataType: 'UUID',
        references: { table: 'innings', column: 'id', onDelete: 'cascade' }
      },
      {
        name: 'secondInnings',
        dataType: 'UUID',
        references: { table: 'innings', column: 'id', onDelete: 'cascade' }
      },
      {
        name: 'thirdInnings',
        dataType: 'UUID',
        references: { table: 'innings', column: 'id', onDelete: 'cascade' }
      },
      {
        name: 'fourthInnings',
        dataType: 'UUID',
        references: { table: 'innings', column: 'id', onDelete: 'cascade' }
      }
    ]
  }),

  ratings: sql.define({
    name: 'ratings',
    columns: [
      { name: 'id', dataType: 'UUID', primaryKey: true, notNull: true },
      {
        name: 'team',
        dataType: 'UUID',
        notNull: true,
        references: { table: 'teams', column: 'id', onDelete: 'cascade' }
      },
      {
        name: 'match',
        dataType: 'UUID',
        notNull: true,
        references: { table: 'matches', column: 'id', onDelete: 'cascade' }
      },
      { name: 'rating', dataType: 'SMALLINT', notNull: true }
    ]
  }),

  predictions: sql.define({
    name: 'predictions',
    columns: [
      { name: 'id', dataType: 'UUID', primaryKey: true, notNull: true },
      { name: 'startTime', dataType: 'TIMESTAMP', notNull: true },
      {
        name: 'format',
        dataType: 'SERIAL',
        notNull: true,
        references: { table: 'formats', column: 'id', onDelete: 'restrict' }
      },
      {
        name: 'home',
        dataType: 'UUID',
        notNull: true,
        references: { table: 'teams', column: 'id', onDelete: 'cascade' }
      },
      {
        name: 'away',
        dataType: 'UUID',
        notNull: true,
        references: { table: 'teams', column: 'id', onDelete: 'cascade' }
      },
      { name: 'winner', dataType: 'CHAR(4)' },
      { name: 'confidence', dataType: 'SMALLINT' },
      {
        name: 'homeInnings',
        dataType: 'UUID',
        references: { table: 'innings', column: 'id', onDelete: 'cascade' }
      },
      {
        name: 'awayInnings',
        dataType: 'UUID',
        references: { table: 'innings', column: 'id', onDelete: 'cascade' }
      }
    ]
  }),

  tweets: sql.define({
    name: 'tweets',
    columns: [
      { name: 'id', dataType: 'UUID', primaryKey: true, notNull: true },
      {
        name: 'prediction',
        dataType: 'UUID',
        notNull: true,
        references: { table: 'predictions', column: 'id', onDelete: 'cascade' }
      },
      { name: 'twitterId', dataType: 'VARCHAR(64)', notNull: true }
    ]
  }),

  metadata: sql.define({
    name: 'metadata',
    columns: [
      { name: 'key', dataType: 'VARCHAR(16)', primaryKey: true, notNull: true },
      { name: 'value', dataType: 'VARCHAR(64)', notNull: true }
    ]
  })
}
