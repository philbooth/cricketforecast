// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const rate = require('./rate')
const { Pool } = require('pg')
const predict = require('./predict')
const Promise = require('bluebird')
const schema = require('./schema')
const twitter = require('../twitter')

const LIMIT_RESULTS = 20

const pool = new Pool()

const ready = Object.keys(schema).reduce((wait, key) => {
  return wait.then(() => pool.query(
    schema[key].create()
      .ifNotExists()
      .toQuery()
  ))
    .then(() => {
      switch (key) {
        case 'teams':
          return pool.query(
            'CREATE INDEX IF NOT EXISTS "trgm_teams_name" ON "teams" USING GIN ("name" gin_trgm_ops);'
          )
        case 'matches':
          return pool.query(
            'CREATE UNIQUE INDEX IF NOT EXISTS "uq_matches_home_away_time_format" ON "matches" ("home", "away", "startTime", "format");'
          )
      }
    })
    .catch(error => {
      console.error(error.message)
      console.error(`Failed to create table "${key}", exiting`)
      pool.end()
        .catch(() => {})
        .then(() => process.exit(1))
    })
}, Promise.resolve())

const { teams, matches, formats, innings, ratings, predictions, tweets, metadata } = schema

module.exports = {
  write (data) {
    return data.reduce((wait, match) => {
      return wait.then(() => {
        let format, homeId, awayId, homeInnings, awayInnings

        const isPrediction = ! match.score

        return ingestFormat(match.format)
          .then(result => {
            format = result.id
            return ingestTeam(match.home)
          })
          .then(result => {
            homeId = result.id
            return ingestTeam(match.away)
          })
          .then(result => {
            awayId = result.id

            return Promise.all([
              selectPrediction(match, format, homeId, awayId),
              selectMatch(match, format, homeId, awayId)
            ])
          })
          .then(([ dbPrediction, dbMatch ]) => {
            if (isPrediction && dbPrediction) {
              return
            }

            if (! isPrediction && dbMatch) {
              return
            }

            console.log(`selecting results for "${match.home.name}" vs "${match.away.name}" ${
              isPrediction ? 'prediction' : 'match'
            }`)
            return Promise.all([ selectTeamResults(homeId), selectTeamResults(awayId) ])
              .then(([ homeResults, awayResults ]) => {
                const homeRating = rate(homeId, format, homeResults.rows)
                const awayRating = rate(awayId, format, awayResults.rows)

                if (isPrediction) {
                  const prediction = predict(homeRating, awayRating)
                  console.log(`prediction for "${match.home.name}" (${homeRating}) vs "${match.away.name}" (${awayRating}):`, prediction)

                  let predictionId
                  // TODO: ingest predicted score
                  return ingestPrediction({ ...match, ...prediction }, format, homeId, awayId)
                    .then(result => {
                      predictionId = result.id
                      return twitter.predictResult(match, prediction)
                    })
                    .then(tweet => ingestTweet(predictionId, tweet.id_str))
                }

                console.log(`score for "${match.home.name}" (${homeRating}) vs "${match.away.name}" (${awayRating}):`, match.score.home.totals, match.score.away.totals)
                return ingestScore(match.score.home, homeId)
                  .then(results => {
                    homeInnings = results.map(scoreResult => scoreResult.id)
                    return ingestScore(match.score.away, awayId)
                  })
                  .then(results => {
                    awayInnings = results.map(matchResult => matchResult.id)
                    return ingestMatch(match, format, homeId, awayId, {
                      home: homeInnings,
                      away: awayInnings
                    })
                  })
                  .then(result => {
                    return ingestRating(homeId, result.id, homeRating)
                      .then(() => ingestRating(awayId, result.id, awayRating))
                  })
                  .then(() => twitter.reportResult(match, dbPrediction))
              })
          })
      }).catch(error => console.error('db error:', error.stack))
    }, ready)
  },

  readMetadata (key) {
    return pool.query(
      metadata.select(metadata.star())
        .where(metadata.key.equals(key))
        .toQuery()
    )
      .then(result => result.rows[0])
  },

  writeMetadata (key, value) {
    return pool.query(
      metadata.insert({
        key,
        value
      })
        .onConflict({
          columns: [ 'key' ],
          update: [ 'value' ]
        })
        .toQuery()
    )
  }
}

const SKIP_WORDS = {
  and: true,
  cc: true,
  ccc: true,
  club: true,
  county: true,
  cricket: true,
  team: true,
  the: true,
  xi: true,
}

function ingestFormat (format) {
  return pool.query(
    formats.select(formats.star())
      .where(formats.name.equals(format))
      .toQuery()
  )
    .then(result => {
      if (result.rows.length > 0) {
        return result
      }

      console.log('inserting format:', format)
      return pool.query(
        formats.insert({ name: format })
          .returning(formats.id)
          .toQuery()
      )
    })
    .then(result => result.rows[0])
}

function ingestTeam (team) {
  return selectTeam(team)
    .then(rows => upsertTeam(team, rows))
}

function selectTeam (team) {
  const queryBase = teams.select(teams.star())
  return Promise.resolve()
    .then(() => {
      if (team.cricbuzzId >= 0) {
        return pool.query(
          queryBase.where(teams.cricbuzzId.equals(team.cricbuzzId))
            .toQuery()
        )
      }
    })
    .then(result => {
      if (result && result.rows && result.rows.length > 0) {
        return result
      }

      return pool.query(
        queryBase.where(teams.name.equals(team.name))
          .toQuery()
      )
    })
    .then(result => {
      if (result && result.rows && result.rows.length > 0) {
        return result
      }

      return pool.query(
        queryBase.where(teams.name.like(`%${team.name}%`))
          .toQuery()
      )
    })
    .then(result => {
      if ((result && result.rows && result.rows.length > 0) || team.name.indexOf(' ') === -1) {
        return result
      }

      const words = team.name.split(' ')
        .map(word => word.trim())
        .filter(word => word.length > 0 && ! SKIP_WORDS[word.toLowerCase()])
        .sort((lhs, rhs) => rhs.length - lhs.length)

      if (words.length === 0) {
        return result
      }

      return Promise.all(words.map(word => pool.query(
        queryBase.where(teams.name.like(`%${word}%`))
          .toQuery()
      )))
        .then(similarResults => ({
          rows: Object.entries(
            similarResults.reduce((matched, similarResult, index) => {
              const word = words[index]

              similarResult.rows.forEach(row => {
                const { id } = row
                const match = matched[id]

                if (match) {
                  match.count += 1
                  match.words.push(word)
                } else {
                  matched[id] = {
                    record: row,
                    count: 1,
                    words: [ word ]
                  }
                }
              })

              return matched
            }, {})
          )
            // eslint-disable-next-line no-unused-vars
            .sort(([ lid, lmatch ], [ rid, rmatch ]) => rmatch.score - lmatch.score)
            // eslint-disable-next-line no-unused-vars
            .map(([ id, match ], index) => {
              console.log(`fuzzy match ${index} for "${team.name}": ${words.join(', ')}`)
              return match.record
            })
        }))
    })
    .then(result => result.rows)
}

function upsertTeam (team, rows) {
  return Promise.resolve()
    .then(() => {
      if (rows.length === 0) {
        console.log(`0 teams found matching "${team.name}", inserting`)

        return pool.query(
          teams.insert({
            id: teams.sql.function('uuid_generate_v4')(),
            name: team.name,
            abbr: team.abbr,
            cricbuzzId: team.cricbuzzId
          })
            .returning(teams.id)
            .toQuery()
        )
      }

      const [ row ] = rows.filter((r, index) => {
        if (index === 0) {
          return true
        }

        console.log(`discarding matched team ${index} for "${team.name}": ${r.name}`)
        return false
      })

      const columns = {}
      let updates = updateColumnIfShorter(team, row, 'name', columns)
      updates += updateColumnIfUnset(team, row, 'abbr', columns)
      updates += updateColumnIfUnset(team, row, 'cricbuzzId', columns)

      if (updates === 0) {
        return pool.query(
          teams.select(teams.id)
            .where(teams.id.equals(row.id))
            .toQuery()
        )
      }

      return pool.query(
        teams.update(columns)
          .where(teams.id.equals(row.id))
          .returning(teams.id)
          .toQuery()
      )
    })
    .then(result => result.rows[0])
}

function updateColumnIfShorter (data, row, key, columns) {
  return maybeUpdateColumn(() => data[key].length > row[key].length, data, key, columns)
}

function updateColumnIfUnset (data, row, key, columns) {
  return maybeUpdateColumn(() => data[key] && ! row[key], data, key, columns)
}

function maybeUpdateColumn (predicate, data, key, columns) {
  if (predicate()) {
    columns[key] = data[key]
    return 1
  }

  return 0
}

function selectMatch (match, format, home, away) {
  const firstInnings = innings.as('fi')
  const secondInnings = innings.as('si')
  const rating = ratings.as('r')
  return pool.query(
    matches.select(
      matches.id,
      firstInnings.id.as('firstInnings'),
      secondInnings.id.as('secondInnings'),
      rating.id.as('rating')
    )
      .from(
        matches
          .leftJoin(firstInnings)
          .on(matches.firstInnings.equals(firstInnings.id))
          .leftJoin(secondInnings)
          .on(matches.secondInnings.equals(secondInnings.id))
          .leftJoin(rating)
          .on(matches.id.equals(rating.match))
      )
      .where(matches.home.equals(home))
      .and(matches.away.equals(away))
      .and(matches.format.equals(format))
      .and(matches.startTime.equals(parseTime(match.time)))
      .toQuery()
  )
    .then(result => result.rows[0])
}

function selectPrediction (match, format, home, away) {
  return pool.query(
    predictions
      .select(
        predictions.id,
        predictions.winner,
        predictions.confidence,
        tweets.twitterId.as('tweetId')
      )
      .from(
        predictions
          .leftJoin(tweets)
          .on(predictions.id.equals(tweets.prediction))
      )
      .where(predictions.home.equals(home))
      .and(predictions.away.equals(away))
      .and(predictions.format.equals(format))
      .and(predictions.startTime.equals(parseTime(match.time)))
      .toQuery()
  )
    .then(result => result.rows[0])
}

function ingestScore (score, teamId) {
  if (score.innings) {
    return Promise.all(score.innings.map(inns => ingestInnings(inns, teamId)))
  }

  return Promise.all([ ingestInnings(score.totals, teamId) ])
}

function ingestInnings (inns, teamId) {
  return pool.query(
    innings.insert({
      id: innings.sql.function('uuid_generate_v4')(),
      team: teamId,
      runs: inns.runs,
      wickets: inns.wickets,
      overs: inns.overs,
      isDeclared: inns.isDeclared,
      isFollowingOn: inns.isFollowingOn
    })
      .returning(innings.id)
      .toQuery()
  )
    .then(result => result.rows[0])
}

function ingestMatch (match, format, home, away, inns) {
  const batFirst = match.score.batFirst || 'home'
  const batSecond = batFirst === 'home' ? 'away' : 'home'

  const [ firstInnings ] = inns[batFirst]
  const [ secondInnings ] = inns[batSecond]
  let [ , thirdInnings ] = inns[batFirst]
  let [ , fourthInnings ] = inns[batSecond]

  if (fourthInnings) {
    const lastInnings = match.score[batSecond].innings[1]
    if (lastInnings && lastInnings.isFollowingOn) {
      const swap = thirdInnings
      thirdInnings = fourthInnings
      fourthInnings = swap
    }
  }

  return pool.query(
    matches.insert({
      id: matches.sql.function('uuid_generate_v4')(),
      startTime: parseTime(match.time),
      format,
      home,
      away,
      toss: match.toss,
      batFirst,
      winner: match.winner,
      isComplete: match.isComplete,
      firstInnings,
      secondInnings,
      thirdInnings,
      fourthInnings
    })
      .returning(matches.id)
      .toQuery()
  )
    .then(result => result.rows[0])
}

function parseTime (timestamp) {
  const time = new Date(timestamp)

  const year = time.getUTCFullYear()
  const month = time.getUTCMonth() + 1
  const day = time.getUTCDate()
  const hour = time.getUTCHours()
  const minute = time.getUTCMinutes()
  const second = time.getUTCSeconds()

  return `${year}-${pad(month)}-${pad(day)} ${pad(hour)}:${pad(minute)}:${pad(second)}`
}

function pad (number) {
  if (number < 10) {
    return `0${number}`
  }

  return number
}

function ingestRating (team, match, rating) {
  return pool.query(
    ratings.insert({
      id: ratings.sql.function('uuid_generate_v4')(),
      team,
      match,
      rating: Math.round(rating)
    })
      .toQuery()
  )
}

function ingestPrediction (match, format, home, away/*, inns*/) {
  //const [ homeInnings, awayInnings ] = inns

  return pool.query(
    predictions.insert({
      id: predictions.sql.function('uuid_generate_v4')(),
      startTime: parseTime(match.time),
      format,
      home,
      away,
      //homeInnings,
      //awayInnings,
      winner: match.winner,
      confidence: match.confidence
    })
      .returning(predictions.id)
      .toQuery()
  )
    .then(result => result.rows[0])
}

function ingestTweet (predictionId, tweetId) {
  return pool.query(
    tweets.insert({
      id: tweets.sql.function('uuid_generate_v4')(),
      prediction: predictionId,
      twitterId: tweetId
    })
      .toQuery()
  )
}

function selectTeamResults (id) {
  const home = teams.as('h')
  const away = teams.as('a')
  const firstInnings = innings.as('i1')
  const secondInnings = innings.as('i2')
  const thirdInnings = innings.as('i3')
  const fourthInnings = innings.as('i4')
  const homeRating = ratings.as('hr')
  const awayRating = ratings.as('ar')
  const results = matches
    .join(home)
    .on(matches.home.equals(home.id))
    .join(away)
    .on(matches.away.equals(away.id))
    .join(firstInnings)
    .on(matches.firstInnings.equals(firstInnings.id))
    .join(secondInnings)
    .on(matches.secondInnings.equals(secondInnings.id))
    .leftJoin(thirdInnings)
    .on(matches.thirdInnings.equals(thirdInnings.id))
    .leftJoin(fourthInnings)
    .on(matches.fourthInnings.equals(fourthInnings.id))
    .leftJoin(homeRating)
    .on(
      home.id.equals(homeRating.team).and(
        matches.id.equals(homeRating.match)
      )
    )
    .leftJoin(awayRating)
    .on(
      away.id.equals(awayRating.team).and(
        matches.id.equals(awayRating.match)
      )
    )
  return pool.query(
    matches.select(
      matches.id,
      home.id.as('home'),
      away.id.as('away'),
      matches.format,
      matches.startTime,
      matches.winner,
      matches.toss,
      matches.batFirst,
      firstInnings.runs.as('i1Runs'),
      firstInnings.wickets.as('i1Wickets'),
      firstInnings.overs.as('i1Overs'),
      secondInnings.runs.as('i2Runs'),
      secondInnings.wickets.as('i2Wickets'),
      secondInnings.overs.as('i2Overs'),
      thirdInnings.runs.as('i3Runs'),
      thirdInnings.wickets.as('i3Wickets'),
      thirdInnings.overs.as('i3Overs'),
      thirdInnings.isFollowingOn.as('i3FollowOn'),
      fourthInnings.runs.as('i4Runs'),
      fourthInnings.wickets.as('i4Wickets'),
      fourthInnings.overs.as('i4Overs'),
      homeRating.rating.as('homeRating'),
      awayRating.rating.as('awayRating')
    )
      .from(results)
      .where(home.id.equals(id))
      .or(away.id.equals(id))
      .and(matches.isComplete.equals(true))
      .order(matches.startTime.descending)
      .limit(LIMIT_RESULTS)
      .toQuery()
  )
}
