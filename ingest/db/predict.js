// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const HOME = 'home'
const AWAY = 'away'
const CONFIDENCE_BOUNDARIES = [
  { base: 0, delta: 18.75 },
  { base: 10, delta: 37.5 },
  { base: 20, delta: 75 },
  { base: 30, delta: 150 },
  { base: 40, delta: 300 },
  { base: 50, delta: 600 },
  { base: 60, delta: 1200 },
  { base: 70, delta: 2400 },
  { base: 80, delta: 4800 }
]

module.exports = predict

function predict (homeRating, awayRating) {
  let winner
  if (homeRating >= awayRating) {
    winner = HOME
  } else if (awayRating > homeRating) {
    winner = AWAY
  }

  const delta = Math.abs(homeRating - awayRating)
  let confidence

  CONFIDENCE_BOUNDARIES.some(boundary => {
    if (delta <= boundary.delta) {
      confidence = boundary.base + Math.floor(delta / (boundary.delta / 20))
      return true
    }
    return false
  })

  if (confidence >= 100) {
    confidence = 99
  }

  // TODO: predict score
  return {
    winner,
    confidence
  }
}
