// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const is = require('check-types')

const { assert } = is

const HOME = 'home'
const AWAY = 'away'

module.exports = rateTeam

function rateTeam (team, format, matches) {
  return matches
    .filter(match => match.format === format)
    .slice(0, 6)
    .reduce((rating, match, index) => {
      const matchRating = rateMatch(team, match)
      // TODO: weight match ratings with date math instead of index
      const matchWeighting = 1 - index / 10
      return rating + matchRating * matchWeighting
    }, 0)
}

function rateMatch (team, match) {
  assertTeam(team, match)
  assertSide(match, 'batFirst')
  if (match.winner) {
    assertSide(match, 'winner')
  }

  const side = team === match.home ? HOME : AWAY
  const otherSide = side === HOME ? AWAY : HOME
  const isWinner = match.winner === side
  const isTied = ! match.winner

  if (isTied) {
    console.log(`zero rating for ${match.home} vs ${match.away} (tied)`, match)
    return 0
  }

  const innings = [ 1, 2, 3, 4 ]
    .map(inningsNumber => marshallInnings(match, inningsNumber))

  let teamScore, oppositionScore
  if (match.batFirst === side) {
    if (innings[2] && innings[2].isFollowingOn) {
      teamScore = aggregateInnings(innings[0], innings[3])
      oppositionScore = aggregateInnings(innings[1], innings[2])
    } else {
      teamScore = aggregateInnings(innings[0], innings[2])
      oppositionScore = aggregateInnings(innings[1], innings[3])
    }
  } else if (innings[2] && innings[2].isFollowingOn) {
    teamScore = aggregateInnings(innings[1], innings[2])
    oppositionScore = aggregateInnings(innings[0], innings[3])
  } else {
    teamScore = aggregateInnings(innings[1], innings[3])
    oppositionScore = aggregateInnings(innings[0], innings[2])
  }

  let rating = teamScore.runs - oppositionScore.runs

  rating += weightedWickets(oppositionScore) - weightedWickets(teamScore)

  if (is.positive(teamScore.overs) && is.positive(oppositionScore.overs)) {
    rating += weightedOvers(oppositionScore) - weightedOvers(teamScore)
  }

  const ratingDifference = (match[`${otherSide}Rating`] || 0) - (match[`${side}Rating`] || 0)
  rating += ratingDifference

  if (isWinner) {
    if (is.lessOrEqual(rating, 0)) {
      return 1
    }
  } else if (is.greaterOrEqual(rating, 0)) {
    return -1
  }

  if (rating === 0) {
    console.log(`zero rating for ${match.home} vs ${match.away} (winner)`, match)
  }

  return rating
}

function assertTeam (team, match) {
  if (team !== match.home && team !== match.away) {
    throw new Error(`Team "${team}" not found in match "${match.id}"`)
  }
}

function assertSide (match, property) {
  const value = match[property]

  if (value !== HOME && value !== AWAY) {
    throw new Error(`Match "${match.id}" has unexpected ${property} property "${value}"`)
  }
}

function marshallInnings (match, inningsNumber) {
  const runs = match[`i${inningsNumber}Runs`]
  const wickets = match[`i${inningsNumber}Wickets`]
  const overs = match[`i${inningsNumber}Overs`]
  const isFollowingOn = !! match[`i${inningsNumber}FollowOn`]

  if (is.greaterOrEqual(runs, 0) && is.greaterOrEqual(wickets, 0)) {
    return {
      runs,
      wickets,
      overs: is.positive(overs) ? overs : null,
      isFollowingOn
    }
  }
}

function aggregateInnings (first, second) {
  const aggregate = { ...first }

  if (second) {
    aggregate.runs += second.runs
    aggregate.wickets += second.wickets

    if (is.positive(aggregate.overs) && is.positive(second.overs)) {
      aggregate.overs += second.overs
    }
  }

  assert.greaterOrEqual(aggregate.runs, 0)
  assert.greaterOrEqual(aggregate.wickets, 0)
  assert.maybe.positive(aggregate.overs)

  return aggregate
}

function weightedWickets (innings) {
  return innings.wickets * 10
}

function weightedOvers (innings) {
  const { overs } = innings
  const floor = Math.floor(overs)
  const fraction = overs % floor
  return Math.round(floor * 10 + fraction * 16.7)
}
