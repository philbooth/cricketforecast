// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const cricapi = require('./cricapi')
const cricbuzz = require('./cricbuzz')
const db = require('./db')
const Promise = require('bluebird')

const MINUTE = 1000 * 60
const HOUR = MINUTE * 60
const DAY = HOUR * 24
const POLL_INTERVAL = HOUR * 6 + MINUTE * 15

setImmediate(run)

function run () {
  fetch()
    .then(store)
    .catch(error => console.error(error.stack))
    .then(() => setTimeout(run, POLL_INTERVAL))
}

function fetch () {
  return Promise.all([ cricapi.fetch(), cricbuzz.fetch() ])
    .then(([ cricapiResult, cricbuzzResult ]) => {
      const predictions = new Set()
      return merge(cricapiResult, cricbuzzResult)
        .filter(match => !! match.format)
        .sort((lhs, rhs) => lhs.time - rhs.time)
        .filter(match => {
          if (match.score) {
            return true
          }

          if (match.time > Date.now() + DAY * 7) {
            // Only predict matches within the next week
            return false
          }

          const homeKey = `${match.home.name}-${match.format}`
          const awayKey = `${match.away.name}-${match.format}`
          if (predictions.has(homeKey) || predictions.has(awayKey)) {
            // Only predict one match ahead for each team:format pair
            return false
          }

          predictions.add(homeKey)
          predictions.add(awayKey)
          return true
        })
    })
}

function store (matches) {
  return db.write(matches)
}

function merge (cricapiResult, cricbuzzResult) {
  const used = {}

  console.log(`merge: ${cricapiResult.length} cricapi results, ${cricbuzzResult.length} cricbuzz results`)

  const merged = cricapiResult.reduce((result, match) => {
    const matched = cricbuzzResult.some((candidate, index) => {
      if (
        ! used[index] && (
          match.home.name.toLowerCase().indexOf(candidate.home.name.toLowerCase()) >= 0 ||
          match.away.name.toLowerCase().indexOf(candidate.away.name.toLowerCase()) >= 0
        ) &&
        match.time === candidate.time &&
        match.format === candidate.format
      ) {
        const mergedItem = Object.assign(candidate, { id: match.id })
        mergedItem.home.name = match.home.name
        mergedItem.away.name = match.away.name
        result.push(mergedItem)
        used[index] = true
        return true
      }

      return false
    })

    if (! matched) {
      result.push(match)
    }

    return result
  }, [])

  if (Object.keys(used).length < cricbuzzResult.length) {
    cricbuzzResult.forEach((match, index) => {
      if (! used[index]) {
        merged.push(match)
      }
    })
  }

  return merged
}
