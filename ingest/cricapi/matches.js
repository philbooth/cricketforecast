// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const Promise = require('bluebird')
const request = require('request-promise')

const {
  CRICAPI_KEY: API_KEY,
  NODE_ENV
} = process.env

const USE_FIXTURE = NODE_ENV === 'development'

if (! USE_FIXTURE && ! API_KEY) {
  throw new Error('Missing API key')
}

const URL = `https://cricapi.com/api/matches?apikey=${API_KEY}`

const FORMATS = new Map([
  [ 'Twenty20', 'T20' ],
  [ 'T20I', 'T20I' ],
  [ 'ODI', 'ODI' ],
  [ 'ListA', '1D' ],
  [ 'First-class', 'FC' ],
  [ 'Test', 'Test' ]
])

module.exports = {
  fetch () {
    return Promise.resolve()
      .then(() => {
        if (USE_FIXTURE) {
          return require('./fixtures/matches-2.json')
        }

        return request(URL)
          .then(result => JSON.parse(result))
      })
      .then(result => result.matches
        .filter(match => {
          if (FORMATS.has(match.type)) {
            return true
          }

          console.log('unhandled cricapi format:', match.type)
          return false
        })
        .map(match => {
          if (match.matchStarted) {
            return marshallMatchWithScore(match)
          }

          return marshallMatchWithoutScore(match)
        })
        .filter(match => match.home.name !== 'TBA' && match.away.name !== 'TBA')
      )
  }
}

function marshallMatchWithScore (match) {
  const { 'team-1': home, 'team-2': away } = match

  return {
    id: match.unique_id,
    time: Date.parse(match.dateTimeGMT),
    home: marshallTeam(home),
    away: marshallTeam(away),
    format: marshallFormat(match.type),
    toss: marshallResult(match.toss_winner_team, home, away),
    winner: marshallResult(match.winner_team, home, away),
    isComplete: !! match.winner_team,
    hasSquads: !! match.squad
  }
}

function marshallMatchWithoutScore (match) {
  const home = marshallTeam(match['team-1'])
  const away = marshallTeam(match['team-2'])
  const format = marshallFormat(match.type)

  return {
    id: match.unique_id,
    time: Date.parse(match.dateTimeGMT),
    home,
    away,
    format
  }
}

function marshallTeam (team) {
  return {
    // Missing: abbr, id
    name: team
  }
}

function marshallFormat (format) {
  return FORMATS.get(format) || format || null
}

function marshallResult (winner, home, away) {
  if (winner === home) {
    return 'home'
  }

  if (winner === away) {
    return 'away'
  }

  return null
}
