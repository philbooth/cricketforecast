// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const matches = require('./matches')
const score = require('./score')

module.exports = {
  fetch () {
    return matches.fetch()
      .catch(error => {
        console.error('cricapi.matches.fetch error:', error.message)
        return []
      })
      .then(results => results
        .map(m => {
          if (! m.winner) {
            return m
          }

          return score.fetch(m.id)
            .then(s => {
              m.score = s
              return m
            })
            .catch(error => {
              console.error('cricapi.score.fetch error:', error.message)
              return m
            })
        })
      )
      .all()
  }
}
