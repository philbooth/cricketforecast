// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const check = require('check-types')
const Promise = require('bluebird')
const request = require('request-promise')

const {
  CRICAPI_KEY: API_KEY,
  NODE_ENV
} = process.env

const USE_FIXTURE = NODE_ENV === 'development'

if (! USE_FIXTURE && ! API_KEY) {
  throw new Error('Missing API key')
}

const BASE_URI = `https://cricapi.com/api/cricketScore?apikey=${API_KEY}&unique_id=`

// https://regex101.com/r/GCelIl/1
// Match group 1: home, team
// Match group 2: home, 2nd or 1st innings, runs
// Match group 3: home, 2nd or 1st innings, wickets
// Match group 4: home, 1st innings, runs
// Match group 5: home, 1st innings, wickets
// Match group 6: home, is batting now?
// Match group 7: away, team
// Match group 8: away, 2nd or 1st innings, runs
// Match group 9: away, 2nd or 1st innings, wickets
// Match group 10: away, 1st innings, runs
// Match group 11: away, 1st innings, wickets
// Match group 12: away, is batting now?
const SCORE = /([\w \t.,'"`:;()&^~=+!-]+) +([0-9]+)\/([0-9]{1,2})(?: +&(?:amp;)? +([0-9]+)\/([0-9]{1,2}))?(?: +(\*))? *v +([\w \t.,'"`:;()&^~=+!-]+) +([0-9]+)\/([0-9]{1,2})(?: +&(?:amp;)? +([0-9]+)\/([0-9]{1,2}))?(?: +(\*))?/

module.exports = {
  fetch (id) {
    return Promise.resolve()
      .then(() => {
        check.assert.positive(id, 'Invalid argument')
        check.assert.integer(id, 'Invalid argument')

        if (USE_FIXTURE) {
          return require('./fixtures/score.json')
        }

        return request(`${BASE_URI}${id}`)
          .then(result => JSON.parse(result))
      })
      .then(s => {
        const parts = SCORE.exec(s.score)
        if (parts && parts.length >= 10) {
          return {
            home: parseScore(parts, 1),
            away: parseScore(parts, 7)
          }
        }

        // HACK: temporary logging to help us understand the possible values here
        console.error('unhandled cricapi score:', s.score, parts)
      })
  }
}

function parseScore (parts, baseIndex) {
  const innings = [ parseInnings(parts, baseIndex + 1) ]
  if (parts[baseIndex + 3]) {
    innings.unshift(parseInnings(parts, baseIndex + 3))
  }

  const totals = innings.reduce((t, i) => {
    t.runs += i.runs
    t.wickets += i.wickets
    return t
  }, {
    runs: 0,
    wickets: 0,
    // TODO: Figure out how to set this, if we can set it
    isDeclared: false
  })

  return {
    team: parts[baseIndex],
    totals,
    innings
  }
}

function parseInnings (parts, baseIndex) {
  return {
    runs: parseInt(parts[baseIndex]),
    wickets: parseInt(parts[baseIndex + 1])
  }
}
