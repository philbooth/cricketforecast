// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const { assert } = require('chai')

/* global suite, setup, test */

suite('db/rate:', () => {
  let rate

  setup(() => {
    rate = require('../../db/rate')
  })

  test('interface was expected', () => {
    assert.isFunction(rate)
    assert.lengthOf(rate, 3)
  })

  test('rate throws if team is not in match', () => {
    assert.throws(() => {
      rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'baz',
          away: 'qux',
          winner: 'away',
          batFirst: 'away',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        }
      ])
    })
  })

  test('rate throws if batFirst is invalid', () => {
    assert.throws(() => {
      rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'hom',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        }
      ])
    })
  })

  suite('rate winning team (home, runs):', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 1)
    })
  })

  suite('rate winning team (home, runs & wickets):', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'away',
          i1Runs: 179,
          i1Wickets: 8,
          i1Overs: 20,
          i2Runs: 180,
          i2Wickets: 7,
          i2Overs: 20
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 11)
    })
  })

  suite('rate winning team (home, runs & overs):', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i1Overs: 19,
          i2Runs: 179,
          i2Wickets: 8,
          i2Overs: 20
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 11)
    })
  })

  suite('rate winning team (home, runs & incomplete overs):', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8,
          i2Overs: 20
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 1)
    })
  })

  suite('rate winning team (home, runs & balls):', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i1Overs: 19.5,
          i2Runs: 179,
          i2Wickets: 8,
          i2Overs: 20
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 3)
    })
  })

  suite('rate winning team (home, runs & rating):', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i1Overs: 20,
          i2Runs: 179,
          i2Wickets: 8,
          i2Overs: 20,
          homeRating: 9,
          awayRating: 10
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 2)
    })
  })

  suite('rate winning team (away):', () => {
    let result

    setup(() => {
      result = rate('foo', 2, [
        {
          format: 2,
          home: 'bar',
          away: 'foo',
          winner: 'away',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i1Overs: 20,
          i2Runs: 181,
          i2Wickets: 7,
          i2Overs: 19.5,
          homeRating: 10,
          awayRating: 10
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 13)
    })
  })

  suite('rate losing team (home):', () => {
    let result

    setup(() => {
      result = rate('wibble', 1, [
        {
          format: 1,
          home: 'wibble',
          away: 'blee',
          winner: 'away',
          batFirst: 'away',
          i1Runs: 180,
          i1Wickets: 8,
          i1Overs: 20,
          i2Runs: 179,
          i2Wickets: 9,
          i2Overs: 20
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, -11)
    })
  })

  suite('rate losing team (away):', () => {
    let result

    setup(() => {
      result = rate('wibble', 2, [
        {
          format: 2,
          home: 'blee',
          away: 'wibble',
          winner: 'home',
          batFirst: 'away',
          i1Runs: 180,
          i1Wickets: 8,
          i1Overs: 20,
          i2Runs: 181,
          i2Wickets: 6,
          i2Overs: 18.1
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, -39)
    })
  })

  suite('rate tie (positive):', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: null,
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i1Overs: 20,
          i2Runs: 180,
          i2Wickets: 8,
          i2Overs: 20,
          homeRating: 10,
          awayRating: 20
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 0)
    })
  })

  suite('rate tie negative:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: null,
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i1Overs: 20,
          i2Runs: 180,
          i2Wickets: 8,
          i2Overs: 20,
          homeRating: 20,
          awayRating: 10
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 0)
    })
  })

  suite('rate winning team with worse wickets:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 9,
          i1Overs: 20,
          i2Runs: 179,
          i2Wickets: 8,
          i2Overs: 20
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 1)
    })
  })

  suite('rate winning team with worse overs:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 10,
          i1Overs: 20,
          i2Runs: 179,
          i2Wickets: 10,
          i2Overs: 19
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 1)
    })
  })

  suite('rate winning team with better rating:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i1Overs: 20,
          i2Runs: 179,
          i2Wickets: 8,
          i2Overs: 20,
          homeRating: 200,
          awayRating: 1
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 1)
    })
  })

  suite('rate losing team with better wickets:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'away',
          batFirst: 'home',
          i1Runs: 179,
          i1Wickets: 8,
          i1Overs: 20,
          i2Runs: 180,
          i2Wickets: 9,
          i2Overs: 20
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, -1)
    })
  })

  suite('rate losing team with better overs:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'away',
          batFirst: 'home',
          i1Runs: 179,
          i1Wickets: 10,
          i1Overs: 19,
          i2Runs: 180,
          i2Wickets: 10,
          i2Overs: 20
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, -1)
    })
  })

  suite('rate losing team with worse rating:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'away',
          batFirst: 'home',
          i1Runs: 179,
          i1Wickets: 8,
          i1Overs: 20,
          i2Runs: 180,
          i2Wickets: 8,
          i2Overs: 20,
          homeRating: 1,
          awayRating: 200
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, -1)
    })
  })

  suite('overwhelming win batting first:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 263,
          i1Wickets: 3,
          i1Overs: 20,
          i2Runs: 39,
          i2Wickets: 10,
          i2Overs: 10.3,
          homeRating: -50,
          awayRating: 60
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 309)
    })
  })

  suite('overwhelming win batting second:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'away',
          i1Runs: 39,
          i1Wickets: 10,
          i1Overs: 10.3,
          i2Runs: 45,
          i2Wickets: 0,
          i2Overs: 1.2,
          homeRating: -50,
          awayRating: 60
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 308)
    })
  })

  suite('rate two matches:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'baz',
          away: 'foo',
          winner: 'away',
          batFirst: 'away',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 1.9)
    })
  })

  suite('rate three matches:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'baz',
          away: 'foo',
          winner: 'away',
          batFirst: 'away',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'foo',
          away: 'qux',
          winner: 'home',
          batFirst: 'away',
          i1Runs: 179,
          i1Wickets: 8,
          i2Runs: 180,
          i2Wickets: 8
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 2.7)
    })
  })

  suite('rate four matches:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'baz',
          away: 'foo',
          winner: 'away',
          batFirst: 'away',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'foo',
          away: 'qux',
          winner: 'home',
          batFirst: 'away',
          i1Runs: 179,
          i1Wickets: 8,
          i2Runs: 180,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'wibble',
          away: 'foo',
          winner: 'away',
          batFirst: 'home',
          i1Runs: 179,
          i1Wickets: 8,
          i2Runs: 180,
          i2Wickets: 8
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result.toPrecision(3), '3.40')
    })
  })

  suite('rate five matches:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'baz',
          away: 'foo',
          winner: 'away',
          batFirst: 'away',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'foo',
          away: 'qux',
          winner: 'home',
          batFirst: 'away',
          i1Runs: 179,
          i1Wickets: 8,
          i2Runs: 180,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'wibble',
          away: 'foo',
          winner: 'away',
          batFirst: 'home',
          i1Runs: 179,
          i1Wickets: 8,
          i2Runs: 180,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'foo',
          away: 'blee',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 4)
    })
  })

  suite('rate six matches:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'baz',
          away: 'foo',
          winner: 'away',
          batFirst: 'away',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'foo',
          away: 'qux',
          winner: 'home',
          batFirst: 'away',
          i1Runs: 179,
          i1Wickets: 8,
          i2Runs: 180,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'wibble',
          away: 'foo',
          winner: 'away',
          batFirst: 'home',
          i1Runs: 179,
          i1Wickets: 8,
          i2Runs: 180,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'foo',
          away: 'blee',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 4.5)
    })
  })

  suite('rate seven matches:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'baz',
          away: 'foo',
          winner: 'away',
          batFirst: 'away',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'foo',
          away: 'qux',
          winner: 'home',
          batFirst: 'away',
          i1Runs: 179,
          i1Wickets: 8,
          i2Runs: 180,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'wibble',
          away: 'foo',
          winner: 'away',
          batFirst: 'home',
          i1Runs: 179,
          i1Wickets: 8,
          i2Runs: 180,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'foo',
          away: 'blee',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 4.5)
    })
  })

  suite('rate wrong format:', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 2,
          home: 'baz',
          away: 'foo',
          winner: 'away',
          batFirst: 'away',
          i1Runs: 180,
          i1Wickets: 8,
          i2Runs: 179,
          i2Wickets: 8
        },
        {
          format: 1,
          home: 'foo',
          away: 'qux',
          winner: 'home',
          batFirst: 'away',
          i1Runs: 179,
          i1Wickets: 8,
          i2Runs: 180,
          i2Wickets: 8
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 1.9)
    })
  })

  suite('rate winning team (home, 2 innings):', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 10,
          i2Runs: 179,
          i2Wickets: 10,
          i3Runs: 180,
          i3Wickets: 10,
          i4Runs: 179,
          i4Wickets: 10
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 2)
    })
  })

  suite('rate winning team (away, 2 innings):', () => {
    let result

    setup(() => {
      result = rate('bar', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'away',
          batFirst: 'home',
          i1Runs: 179,
          i1Wickets: 10,
          i2Runs: 180,
          i2Wickets: 10,
          i3Runs: 179,
          i3Wickets: 10,
          i4Runs: 180,
          i4Wickets: 10
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 2)
    })
  })

  suite('rate winning team (2 innings, innings victory):', () => {
    let result

    setup(() => {
      result = rate('bar', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'away',
          batFirst: 'home',
          i1Runs: 179,
          i1Wickets: 10,
          i2Runs: 180,
          i2Wickets: 10,
          i3Runs: 0,
          i3Wickets: 10
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 101)
    })
  })

  suite('rate winning team (2 innings with follow on):', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 10,
          i2Runs: 179,
          i2Wickets: 10,
          i3Runs: 179,
          i3Wickets: 10,
          i3FollowOn: true,
          i4Runs: 180,
          i4Wickets: 10
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 2)
    })
  })

  suite('rate winning team (2 innings with follow on and innings victory):', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 180,
          i1Wickets: 10,
          i2Runs: 29,
          i2Wickets: 10,
          i3Runs: 150,
          i3Wickets: 10,
          i3FollowOn: true
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 101)
    })
  })

  suite('rate winning team (2 innings with huge margin):', () => {
    let result

    setup(() => {
      result = rate('foo', 1, [
        {
          format: 1,
          home: 'foo',
          away: 'bar',
          winner: 'home',
          batFirst: 'home',
          i1Runs: 800,
          i1Wickets: 4,
          i2Runs: 80,
          i2Wickets: 10,
          i3Runs: 150,
          i3Wickets: 10,
          i3FollowOn: true
        }
      ])
    })

    test('result was correct', () => {
      assert.equal(result, 730)
    })
  })
})
