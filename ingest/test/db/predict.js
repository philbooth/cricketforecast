// Copyright © 2018, 2019 Phil Booth
//
// This file is part of cricketforecast.
//
// cricketforecast is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cricketforecast is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with cricketforecast. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const { assert } = require('chai')

/* global suite, setup, test */

suite('db/predict:', () => {
  let predict

  setup(() => {
    predict = require('../../db/predict')
  })

  test('interface was expected', () => {
    assert.isFunction(predict)
    assert.lengthOf(predict, 2)
  })

  suite('predict home winner:', () => {
    let result

    setup(() => {
      result = predict(100, 99)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'home',
        confidence: 1
      })
    })
  })

  suite('predict away winner:', () => {
    let result

    setup(() => {
      result = predict(99, 100)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'away',
        confidence: 1
      })
    })
  })

  suite('predict tie:', () => {
    let result

    setup(() => {
      result = predict(0, 0)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'home',
        confidence: 0
      })
    })
  })

  suite('predict tiny winner:', () => {
    let result

    setup(() => {
      result = predict(0, 1)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'away',
        confidence: 1
      })
    })
  })

  suite('predict small winner:', () => {
    let result

    setup(() => {
      result = predict(150, 0)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'home',
        confidence: 50
      })
    })
  })

  suite('predict less small winner:', () => {
    let result

    setup(() => {
      result = predict(151, 0)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'home',
        confidence: 50
      })
    })
  })

  suite('predict small-ish winner:', () => {
    let result

    setup(() => {
      result = predict(300, 0)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'home',
        confidence: 60
      })
    })
  })

  suite('predict less small-ish winner:', () => {
    let result

    setup(() => {
      result = predict(301, 0)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'home',
        confidence: 60
      })
    })
  })

  suite('predict moderate winner:', () => {
    let result

    setup(() => {
      result = predict(600, 0)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'home',
        confidence: 70
      })
    })
  })

  suite('predict less moderate winner:', () => {
    let result

    setup(() => {
      result = predict(601, 0)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'home',
        confidence: 70
      })
    })
  })

  suite('predict big winner:', () => {
    let result

    setup(() => {
      result = predict(1200, 0)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'home',
        confidence: 80
      })
    })
  })

  suite('predict bigger winner:', () => {
    let result

    setup(() => {
      result = predict(1201, 0)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'home',
        confidence: 80
      })
    })
  })

  suite('predict huge winner:', () => {
    let result

    setup(() => {
      result = predict(2400, 0)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'home',
        confidence: 90
      })
    })
  })

  suite('predict more huge winner:', () => {
    let result

    setup(() => {
      result = predict(2401, 0)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'home',
        confidence: 90
      })
    })
  })

  suite('predict dead cert:', () => {
    let result

    setup(() => {
      result = predict(4800, 0)
    })

    test('result was correct', () => {
      assert.deepEqual(result, {
        winner: 'home',
        confidence: 99
      })
    })
  })
})
