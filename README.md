# Cricket Forecast

A bot that predicts
the outcomes of cricket matches
based on previous results.

Predictions are published
on [https://www.cricketforecast.com/](https://www.cricketforecast.com/)
and [@CricketForecast](https://twitter.com/CricketForecast).
